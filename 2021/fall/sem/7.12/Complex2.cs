﻿using System;
namespace slok
{
    class Program
    {
        static void Main()
        {
            int n = 6;
            var a = new OpenSegment[]
            {
               new OpenSegment(1,2),
               new OpenSegment(9,11),
               new OpenSegment(1,4),
               new OpenSegment(8, 11)
            };
            Sort(a);
            FindCrossing(a);
        }

        public static void FindCrossing(OpenSegment[] a)
        {
            var result = new OpenSegment(0, 0);
            int min = int.MaxValue;
            bool hasCrossing = false;
            for (int i = 0; i < a.Length; i++)
            {
                for (int j = i + 1; j < a.Length; j++)
                {
                    var currentLen = a[i].GetComb(a[j]);
                    if (currentLen < min && currentLen != 0)
                    {
                        min = currentLen;
                        result.Begin = a[Math.Max(i, j)].Begin;
                        result.End = a[Math.Min(i, j)].End;
                        hasCrossing = true;
                    }
                }
            }
            if (hasCrossing)
                Print(result);
            else
                Print(hasCrossing);
        }

        public static void Print(object toPrint)
        {
            System.Console.WriteLine(toPrint);
        }

        public static void Sort(OpenSegment[] array)
        {
            for (int i = array.Length - 1; i > 0; i--)
                for (int j = 1; j <= i; j++)
                {
                    OpenSegment element1 = array[j - 1];
                    OpenSegment element2 = array[j];
                    if (element1.CompareTo(element2) < 0)
                    {
                        object temporary = array[j];
                        array.SetValue(array[j - 1], j);
                        array.SetValue(temporary, j - 1);
                    }
                }
        }
    }
    public class OpenSegment : IComparable
    {
        public int Begin;
        public int End;

        public override string ToString()
        {
            return string.Format("({0};{1})", Begin, End); 
        }
        public  OpenSegment(int begin, int end)
        {
            Begin = begin;
            End = end;
        }

        public int GetComb(OpenSegment another)
        {
            
            int c = Math.Min(this.End, another.End) - Math.Max(this.Begin, another.Begin);
            if (c < 0 || this.End <= another.Begin || another.End <= this.Begin) return 0;
            return (int)c - 1;
        }
        public int CompareTo(object obj)
        {
            var segment = (OpenSegment)obj;
            if (this.Begin == segment.Begin)
                return -this.End.CompareTo(segment.End);
            return -this.Begin.CompareTo(segment.Begin);
        }
    }
}