﻿using System;

namespace WorkingSpace

{
    public class Program
    {
        static void Main()
        {
            Console.Write(PowerNumber(0, 0));
        }

        public static double PowerNumber(double number, int pow)
        {
            if (pow>=0)
            {
                double result = 1;
                while (pow != 0)
                {
                    if (pow % 2 == 1)
                    {
                        result *= number;
                        --pow;
                    }
                    number *= number;
                    pow /= 2;
                }
                return result;
            }
            return -1;
        }
    }
}