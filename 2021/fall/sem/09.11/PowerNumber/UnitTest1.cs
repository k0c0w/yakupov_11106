using NUnit.Framework;
using WorkingSpace;

namespace TestProject3
{
    public class Tests
    {

        [TestCase(2,3,8)]
        [TestCase(-2,2,4)]
        [TestCase(0,5,0)]
        [TestCase(7,-8,-1)]
        [TestCase(1.5,2,2.25)]

        public static void PowerNumberTest(double number, int power,double expected) 
        {
            var result = Program.PowerNumber(number, power);

            Assert.AreEqual(expected, result);
        }
    }
}