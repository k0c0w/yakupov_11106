﻿using System;
using System.Collections.Generic;


namespace WorkingSpace

{
    public class Program
    {
        static void Main()
        {
            int n = int.Parse(Console.ReadLine());
            Print(FindAllQueenCombinations(n),n);
        }

        public static List<int[]> FindAllQueenCombinations(int n)
        {
            var allComobinations = new List<int[]>();
            var board = new int[n*n];
            CombineQueenPositions(board,0,n,0, allComobinations);
            return allComobinations;
        }

        static void Print(List<int[]> a, int n)
        {
            Console.WriteLine();
            foreach(var solve in a)
            {
                for (int x=0; x< n; x++)
                {
                    for (int y=0; y<n; y++)
                    {
                        Console.Write("{0,-5}", solve[x * n + y]);
                    }
                    Console.WriteLine();
                }
                Console.Write("\n\n\n");
            }
        }

        public static void CombineQueenPositions(int[] board, int currentQueen, int n,int position, List<int[]> storage)
        {
            if (currentQueen == n)
            {
                if (IsValidBoard(board, n))
                {
                    var result = new int[n*n];
                    board.CopyTo(result, 0);
                    storage.Add(result);
                }
                return;
            }
            if (position == n * n || !IsValidBoard(board,n))
                return;
            board[position] = 1;
            CombineQueenPositions(board, currentQueen + 1, n, position+1, storage);
            board[position] = 0;
            CombineQueenPositions(board, currentQueen, n, position + 1, storage);
        }
        public static bool IsValidBoard (int[] board,int n)
        {
            for (int x= 0; x<n; x++)
            {
                for (int y =0; y<n; y++)
                {
                    if (board[x*n+y] == 1 && AttackOther(x, y, board, n))
                        return false;
                }
            }
            return true;
        }
        // здесь можно попытаться оптимизировать, так как раз точку (a,b) не бьет из позиции (x,y) то соответсвенно точку 
        // (x,y) не бьет из позиции (a,b)
        public static bool AttackOther(int x1, int y1, int[] board, int n)
        {
            for (int x2 = x1; x2 < n; x2++)
            {
                for (int y2 = y1; y2 < n; y2++)
                {
                    //if (x1 == x2 && y1 == y2)
                    //    continue;
                    if (board[x2 * n + y2] == 1)
                    {
                        bool horizontalAttack = x1 == x2;
                        bool verticalAttack = y1 == y2;
                        bool diagonalAttack = Math.Abs(x1 - x2) == Math.Abs (y1 - y2);
                        if (horizontalAttack || verticalAttack || diagonalAttack)
                            return true;
                    }
                }
            }
            return false;
        }
    }
}