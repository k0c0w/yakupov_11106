using NUnit.Framework;
using WorkingSpace;
using System.Collections.Generic;

namespace TestProject3
{
    public class Tests
    {
        [TestCase(1,1)]
        [TestCase(2,0)]
        [TestCase(3,0)]
        [TestCase(4,2)]
        public static void FindQueenCombinationsTest(int n, int expectedCount)
        {

            var result = Program.FindAllQueenCombinations(n).Count;

            Assert.AreEqual(expectedCount, result);
        }
        public static void CombineQueenPositions()
        {

        }
        
        [TestCase(true, 1 ,new int[] { 1 })]
        [TestCase(true, 4,
            new int[]
            { 0,1,0,0,
              0,0,0,1,
              1,0,0,0,
              0,0,1,0
            })
        ]
        [TestCase(false, 2, new int[] {1,0,0,1})]
        public static void IsValidBoardTest(bool expected, int n, int[] board) 
        {
            bool result = Program.IsValidBoard(board, n);

            Assert.AreEqual(expected, result);
        }

        [Test]
        public static void AttackOtherTest()
        {
            var board = new int[]
            {
                1,0,0,
                0,0,1,
                1,0,0
            };
            int n = 3;
            int x1 = 0;
            int y1 = 0;
            bool expected = true;

            bool result = Program.AttackOther(x1, y1, board, n);

            Assert.AreEqual(expected, result);
        }
    }
}