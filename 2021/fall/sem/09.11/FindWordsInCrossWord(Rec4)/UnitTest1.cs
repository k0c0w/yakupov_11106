using NUnit.Framework;
using WorkingSpace;
using System.Collections.Generic;

namespace TestProject3
{
    public class Tests
    {
        [TestCase("m", new string[] { "m" }, 1, true)]
        [TestCase("car", new string[] {"ca","sr"}, 2, true)]
        [TestCase("moon", new string[] {"ca","sr"}, 2, false)]
        [TestCase("as", new string[] { "al", "sr" }, 2, true)]
        public static void IsInCrosswordTest(string word, string[] crossword, int n, bool expected)
        {
            bool result = Program.IsInCrossword(word, crossword, n);

            Assert.AreEqual(expected, result);
        }

        [Test]
        public static void FindWordsInCrosswordTest()
        {
            var crossword = new string[]
            {
                "abra",
                "adac",
                "babr",
                "arca"
            };
            int n = 4;
            var wordStorage = new string[]
            {
                "abracadabra",
                "ababaab",
                "ababaaba"
            };
            var valueStorage = new string[]
            {
                "NO",
                "NO",
                "NO"
            };
            var wordsValuesExpected = new string[]
            {
                "YES",
                "YES",
                "NO"
            };

            Program.FindWordsInCrossword(crossword, wordStorage, valueStorage, n);

            for (int i = 0; i < 3; i++)
                Assert.AreEqual(wordsValuesExpected[i], valueStorage[i]);
        }
    }
}