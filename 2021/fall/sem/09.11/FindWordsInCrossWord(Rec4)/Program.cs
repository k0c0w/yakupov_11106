﻿using System;

namespace WorkingSpace
{
    public class Program
    {

        static void Main()
        {
            int n = 4;
            var crossword = new string[n];
            for (int line = 0; line < n; line++)
                crossword[line] = Console.ReadLine();

            int wordsCount = int.Parse(Console.ReadLine());
            var wordStorage = new string[wordsCount];
            var valueStorage = new string[wordsCount];
            for (int word = 0; word < wordsCount; word++)
            {
                wordStorage[word] = Console.ReadLine();
                valueStorage[word] = "NO";
            }

            FindWordsInCrossword(crossword, wordStorage, valueStorage, n);

            for (int i =0; i< wordsCount; i++)
            {
                Console.WriteLine("{0}: {1}", wordStorage[i], valueStorage[i]);
            }
        }

        public static void FindWordsInCrossword(string[] crossword, string[] wordStorage, string[] valueStorage, int n)
        {
            for (int i = 0; i < wordStorage.Length; i++)
            {
                if (IsInCrossword(wordStorage[i], crossword, n))
                    valueStorage[i] = "YES";
            }
        }
        public static  bool IsInCrossword(string word, string[] crossword, int n)
        {
            var wordIn = new bool[] { false };
            var path = new int[n*n];
            for ( int i =0; i<n*n; i++)
            {
                int x = i / n;
                int y = i % n;
                PermutateWords(x,y, word,wordIn,path, crossword, n, 0);
                if (wordIn[0])
                    break;
            }
            return wordIn[0];
        }

        public static void PermutateWords(int x, int y, string word, bool[] wordIn, int[] path, string[] crossword, int n, int wordPosition)
        {
            if (wordIn[0])
                return;
            if (wordPosition == word.Length)
            {
                wordIn[0] = true;
                return;
            }
            bool correctPosition = x != -1 && x != n && y != -1 && y != n;
            if (correctPosition && path[x*n+y] != 1 && word[wordPosition] == crossword[x][y])
            {
                path[x * n + y] = 1;
                PermutateWords(x + 1, y, word, wordIn, path, crossword, n, wordPosition + 1);
                PermutateWords(x, y + 1, word, wordIn, path, crossword, n, wordPosition + 1);
                PermutateWords(x - 1, y, word, wordIn, path, crossword, n, wordPosition + 1);
                PermutateWords(x, y - 1, word, wordIn, path, crossword, n, wordPosition + 1);
                path[x * n + y] = 0;
            }

        }
    }
}