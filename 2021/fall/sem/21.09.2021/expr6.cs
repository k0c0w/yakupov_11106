﻿using System;


namespace sem
{
    class expr6
    {
        static void CalculateDistance(double x0, double y0, double xl1, double yl1, double xl2, double yl2)
        {
            double distance;
            if (xl2 != xl1 & yl1 != yl2)
            {
                double k = (yl2 - yl1) / (xl2 - xl1);
                double c = yl1 - k * xl1;
                double k2 = -1 / k;
                double c2 = y0 - k2 * x0;
                double xd = (c - c2) / (k2 - k);
                double yd = k * xd + c;
                distance = Math.Sqrt((y0 - yd) * (y0 - yd) + (x0 - xd) * (x0 - xd));
            }
            else if (xl2 == xl1 & yl2 != yl1)
            {
                distance = Math.Abs(x0 - xl2);
            }
            else if (xl2 != xl1 & yl1 == yl2)
            {
                distance = Math.Abs(y0 - yl2);
            }
            else distance = double.NaN;
            Console.WriteLine(distance);
        }
    
        static void Main(string[] args)
        {
            CalculateDistance(0,0, 1, 1, 1, -5);
        }
    }
}
