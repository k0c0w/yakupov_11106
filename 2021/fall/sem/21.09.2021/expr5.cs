﻿using System;


namespace sem
{
    class expr5
    {
        static void CalculateLeapYears(int start, int stop)
        {
            var result = (stop / 4 - stop / 100 + stop / 400) - (start / 4 - start / 100 + start / 400);
            if (start == 0 || stop == 0)
                result += 1;
            Console.WriteLine(result);
        }
        static void Main(string[] args)
        {
            CalculateLeapYears(4, 8);
        }
    }
}
