﻿using System;

namespace Testspace
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array = new int[6] { 0,1,2,3,5,5};
            int[] array2 = new int[3] { 0,3 , 5 };
            
            PrintArray(UniteArrays(array, array2));
            PrintArray(CrossArrays(array, array2));
            PrintArray(StripArrays(array, array2));

        }

       static int[] UniteArrays(int[] array1, int[] array2)
        {
            int[] union = new int[array1.Length + array2.Length];
            for (int i = 0; i < array1.Length; i++)
                union[i] = array1[i];
            for (int i = 0; i < array2.Length; i++)
                union[array1.Length + i] = array2[i];
            for (int i =0; i<array1.Length+array2.Length;i++)
            {
                for (int j = array1.Length + array2.Length - 1; j>i; j--)
                {
                    if (union[j - 1] > union[j])
                    {
                        int memory = union[j - 1];
                        union[j - 1] = union[j];
                        union[j] = memory;
                    }
                }
            }
            return union;
        }

        static int[] CrossArrays(int[] array1,int[] array2)
        {
            int[] crossfection = new int[CalculateCrossfectionLenght(array1,array2)];
            int k = 0;
            for (int i =0; i< array1.Length; i++)
            {
                int count = 0;
                for(int j =0; j<array2.Length; j++)
                {
                    if (array1[i] == array2[j])
                        ++count;
                }
                if (count != 0)
                {
                    crossfection[k] = array1[i];
                    ++k;
                }

            }
            return crossfection;
        }

        static int[] StripArrays(int[] maxArray, int[] minArray)
        {
            int[] union = UniteArrays(maxArray,minArray);
            int[] stripped = new int[maxArray.Length + minArray.Length];
            int i = 0;
            int currentStrip = 0;
            while (i < stripped.Length)
            {
                int k = i;
                while(k<union.Length-1 && union[k] == union[k+1])
                {
                    ++k;
                }
                if ((k-i+1) % 2 == 1)
                {
                    stripped[currentStrip] = union[i];
                    ++currentStrip;
                }
                i = k;
                ++i;
            }
            int length = 0;
            foreach (int item in stripped)
            {
                if (item != 0)
                    ++length;
            }
            int[] clearStripped = new int[length];
            for (i = 0; i < length; i++)
                clearStripped[i] = stripped[i];
            return clearStripped;

        }
        static int CalculateCrossfectionLenght(int[] array1, int[] array2)
        {
            int lenght = 0;
            for (int i = 0; i < array1.Length; i++)
            {
                int count = 0;
                for (int j = 0; j < array2.Length; j++)
                {
                    if (array1[i] == array2[j])
                        ++count;
                }
                if (count != 0)
                    ++lenght;
            }
            return lenght;
        }
        static void ShiftElements(int[] array,int k)
        {
            int memory = array[0];
            array[0] = array[array.Length  - k];
            int memory2;
            for (int i =1; i<array.Length; i++)
            {
                memory2 = array[i];
                array[(i+k)%array.Length] = memory;
                memory = memory2;
            }
        }

        static void PrintArray(int[] array)
        {
            foreach (int num in array)
                Console.Write(" {0} ",num);
            Console.WriteLine();
        }
    }
}