﻿using System;

namespace TestSpace
{
    class Program
    {
        static void Main(string[] args)
        {
            var array = new int[5] {0,1,2,3,4};
            MoveElements(array, 2);
            foreach (var item in array)
                Console.Write("{0,-3 }", item);
        }

        static void MoveElements(int[] array, int positionsCount)
        {
            Array.Reverse(array, 0, positionsCount);
            Array.Reverse(array, positionsCount, array.Length - positionsCount);
            Array.Reverse(array, 0, array.Length);
        }
    }
}