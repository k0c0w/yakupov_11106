﻿using System;
using System.Collections.Generic;

namespace TestSpace
{
    public class Program
    {
        static void Main()
        {
            int p = int.Parse(Console.ReadLine());
            var period = FindSequencePeriod(p);
            Console.WriteLine(period);
        }
        public static  int FindSequencePeriod(int primeModule)
        {
            var FibonacciTypeSeq = new List<int>() { 0, 1 };
            int periodCount = 2;
            int i = 2;
            if (primeModule == 0)
                periodCount = 0;
            else if (primeModule == 1)
                periodCount = 1;
            else
            {
                while (true)
                {
                    FibonacciTypeSeq.Add((FibonacciTypeSeq[i - 2] + FibonacciTypeSeq[i - 1]) % primeModule);
                    ++periodCount;
                    if (FibonacciTypeSeq[i] == 1 && FibonacciTypeSeq[i - 1] == 0)
                    {
                        periodCount -= 2;
                        break;
                    }
                    ++i;
                }
            }
            return periodCount;
        }
    }
}