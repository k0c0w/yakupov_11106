using NUnit.Framework;

namespace TestProject3
{
    public class Tests
    {

        [TestCase(0,0)]
        [TestCase(1,1)]
        [TestCase(7,16)]
        [TestCase(15,40)]
        public void FindPeriodSeqTest(int p, int expectedResult)
        {
            var result = 0;

            result = TestSpace.Program.FindSequencePeriod(p);

            Assert.AreEqual(expectedResult, result);
        }
    }
}