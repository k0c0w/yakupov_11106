﻿using System;
using System.Linq;

namespace WorkingSpace
{
    class CalculatingSkierPlaceTask
    {
        static void Main()
        {
            int n = int.Parse(Console.ReadLine());
            int[] winnersLine = Console.ReadLine().Split().Select(int.Parse).ToArray();
            int[,] place = CalculatePossiblePlace(n, winnersLine);

            for (int i=0; i<n; i++)
            {
                Console.WriteLine("{0} {1}", place[i, 0], place[i, 1]);
            }
        }

        static int [,] CalculatePossiblePlace(int n, int[] input)
        {
            int[,] place = new int[n, 2];
            int[] overtakingCounts = new int[2*n];
            for (int i = 0; i < n; i++)
            {
                place[i, 0] = 1;
                place[i, 1] = n;
                overtakingCounts[i] = 0;
            }
            for (int i =0;i<n; i++)
            {
                int currentMan = input[i];
                int manIndex = currentMan - 1;
                for (int j=0; j<n; j++)
                {
                    if (j < i && input[j] > currentMan)
                        ++overtakingCounts[manIndex]; //отстал
                    else if (i < j && input[j] < currentMan)
                        --overtakingCounts[manIndex + n];
                }
                place[manIndex, 0] += overtakingCounts[manIndex];
                place[manIndex, 1] += overtakingCounts[manIndex + n];
            }
            return place;
        }
    }
}