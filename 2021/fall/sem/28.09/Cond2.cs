﻿using System;

namespace TestSpace
{
    class Cond2
    {
        
        static void Main(string[] args)
        {
            double[] input = Array.ConvertAll<string, double>(Console.ReadLine().Split(), Double.Parse);
            double x = input[0], y = input[1], z = input[2], a = input[3], b = input[4];
            Console.WriteLine(CanPassTrough(x, y, z, a, b));
        }
        static bool CanPassTrough(double x, double y, double z, double a, double b)
        {
            double minHole = Math.Min(a, b);
            double maxHole = Math.Max(a, b);
            return minHole >= Math.Min(x, Math.Min(y, z)) && maxHole >= Math.Max(x, Math.Max(y,z));
        }
    }
}
