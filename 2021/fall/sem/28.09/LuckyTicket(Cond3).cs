﻿using System;

namespace TestSpace
{
    class Cond3
    {
        static int SumNums(int a)
        {
            int s = 0;
            while (a != 0)
            {
                s += a % 10;
                a /= 10;
            }
            return s;
        }
        static bool IsLucky(int a)
        {
            return SumNums(a / 1000) == SumNums(a % 1000);
        }
        static void Main(string[] args)
        {
            int n = Convert.ToInt32(Console.ReadLine());
            if (IsLucky(n-1)|| IsLucky(n+1))
                Console.WriteLine("Yes");
            else
                Console.WriteLine("No");
        }    
    }
}
