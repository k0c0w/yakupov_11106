﻿using System;

namespace TestSpace
{
    class Chess
    {
        
        static void Main(string[] args)
        {
            Console.WriteLine(IsCorrectMove("a1", "b2", "bishop"));
        }

        public static bool IsCorrectMove(string from, string to, string figure)
        {
            var dx = Math.Abs(to[0] - from[0]); //смещение фигуры по горизонтали
            var dy = Math.Abs(to[1] - from[1]); //смещение фигуры по вертикали
            switch (figure) 
            {
                case "queen":
                    return dx == dy && dx != 0 || dx == 0 && dx != dy || dy == 0 && dy != dx;
                case "king":
                    return (dx * dx + dy * dy) <= 2 && (dx != 0 || dy != 0);
                case "rook":
                    return dy != dx && (dy == 0 || dx == 0);
                case "bishop":
                    return dx == dy && dx != 0;
                case "knight":
                    return dx * dx + dy * dy == 4;
                default:
                    throw new Exception("unknown figure");
        }
        }
            
    }
}
