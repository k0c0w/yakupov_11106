﻿using System;

namespace TestSpace
{
    class Program
    {
        
        static void Main(string[] args)
        {
            double x1, x2, x3, y1, y2, y3,x4 = Double.NaN,y4=Double.NaN;
            x1 = 0;
            y1 = 0;
            x2 = 0;
            y2 = 1;
            x3 = 1;
            y3 = 0;
            Console.WriteLine(IsSquare(x1,y1,x2,y2,x3,y3));
            if (IsSquare(x1, y1, x2, y2, x3, y3))
            {
                bool normalAngleX1Y1 = 0 == ((x2 - x1) * (x3 - x1) + (y2 - y1) * (y3 - y1));
                bool normalAngleX2Y2 = 0 == ((x1 - x2) * (x3 - x2) + (y1 - y2) * (y3 - y2));
                bool normalAngleX3Y3 = 0 == ((x2 - x3) * (x1 - x3) + (y2 - y3) * (y1 - y3));
                if (normalAngleX1Y1)
                {
                    x4 = (x2 + x3) - x1;
                    y4 = (y2 + y3) - y1;
                }
                else if (normalAngleX2Y2)
                {
                    x4 = (x1 + x3) - x2;
                    y4 = (y1 + y3) - y2;
                }
                else if (normalAngleX3Y3)
                {
                    x4 = (x2 + x1) - x3;
                    y4 = (y2 + y1) - y3;
                }
                Console.WriteLine("({0};{1})", x4, y4);
            }
            else
                Console.WriteLine("Impossible to calculate (x4;y4)");
            

        }

        public static bool IsSquare(double x1, double y1,  double x2, double y2, double  x3, double y3)
        {
            double a = (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2);
            double b = (x1 - x3) * (x1 - x3) + (y1 - y3) * (y1 - y3);
            double c = (x3 - x2) * (x3 - x2) + (y3 - y2) * (y3 - y2);// sides in second pow
            if ((a == b || a == c || b == c) && Math.Max(a, Math.Max(b, c)) == 2 * Math.Min(a, Math.Min(b, c)))
            {
                return true;
            }    
            return false;       
        }
            
    }
}
