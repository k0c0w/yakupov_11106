﻿using System;

namespace TestSpace
{
    class Cond4
    {
       static void Main(string[] args)
        {

            double[] input = Array.ConvertAll<string, double>(Console.ReadLine().Split(), Convert.ToDouble);
            double a = input[0], b = input[1], c = input[2], d = input[3];
            if (b<c || d<a)
		Console.WriteLine("None");
	    else
            	Console.WriteLine("[{0};{1}]", (a >= c) ? a : c, (d >= b) ? b : d);
        }    
    }
}
