﻿using System;

namespace TestSpace
{
    class IsCorrectSequence
    {
        static void Main(string[] args)
        {
            string input = Console.ReadLine();
            Console.WriteLine(IsCorrectString(input));
            Console.WriteLine(FindMaxParenthesisCount(input));
        }

        static bool IsCorrectString(string input)
        {
            int count = 0;
            for (int i = 0; i < input.Length; i++)
            {
                if (input[i] == '(')
                    ++count;
                if (input[i] == ')')
                    --count;
                if (count < 0)
                    return false;
            }
            if (count == 0)
                return true;
            return false;
        }
        static int FindMaxParenthesisCount(string input)
        {
            if (!IsCorrectString(input))
                return 0;
            int currentDepth = 0;
            int maxDepth = 0;
            for (int i =0; i < input.Length; i++)
            {
                if (input[i] == '(')
                    ++currentDepth;
                maxDepth = Math.Max(maxDepth, currentDepth);
                if (input[i] == ')')
                    --currentDepth;
            }
            return maxDepth;
        }
    }
}
