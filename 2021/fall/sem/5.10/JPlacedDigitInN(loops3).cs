﻿using System;

namespace TestSpace
{
    class Cond4
	{ 
		static void Main(string[] args)
        {
         
                FindDigit(50);
		}   
        
        static void FindDigit(int placeNumber)
        {
            int i = 1; // counts of digits in number
            int s = 9;
            while (s < placeNumber)
            {
                placeNumber -= s * i;
                ++i;
                s *= (10 * i);
            }
            double iDigitNumber = 1;
            for (int j = 1; j < i; j++)
                iDigitNumber *= 10;
            double number =iDigitNumber + Math.Ceiling((double)placeNumber/i) - 1;
            placeNumber -= i * (int)(number - iDigitNumber) +1; 
            Console.Write/*Line*/(number.ToString()[placeNumber]);
        }
    }
}
