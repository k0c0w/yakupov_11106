﻿using System;

namespace sem
{
    class Class1
    {
           static void Main(string[] args)
        {
            expr13();
        } 
        static void expr13()
        {
            string[] input = Console.ReadLine().Split();
            double a = Convert.ToDouble(input[0]);
            double r = Convert.ToDouble(input[1]);
            double result;
            //если можно вписать очевидно
            if (r <= a / 2) result =Math.PI * r * r;
            else if (r >= a / Math.Sqrt(2)) result = a * a;//если радиус боьше описанной, то сьест все
            else
            {
                //длина отрезка от угла до точки пересечения окружности и стороны квадрата
                double x = a / 2 - Math.Sqrt(r * r - a * a / 4);
                //area of circle`s sector; simplified fraction; used - cos(x) theorem
                double areaOfCircleSector = r * r * Math.Acos(1 - x * x / (r * r)) / 2;
                //area of sector without segment with radius r and chord of circle with length (a-2x); simplified
                //used cos(x) theorem  and S_triangle r*r*sin(b)/2
                double chord = a - 2 * x;
                double usedForMathAngel = Math.Acos(1 - chord * chord / (2 * r * r));
                double areaOfCircleTriangle = r * r * Math.Sin(usedForMathAngel) / 2;
                result = 4 * (areaOfCircleSector + areaOfCircleTriangle);
            }
            Console.WriteLine("{0:0.###}",result);//када нельзя вписать круг - казус
        }
    }
}
