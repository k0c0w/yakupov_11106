﻿using System;

namespace sem
{
    class Class1
    {
        static void Plane(int h, int t, int v, int x)
        {
            double maxTime;
            double minTime;
            if (h / t > x)
            {
                maxTime = (double)t;
                minTime = (double)(h - x * t) / (v - x);
            }
            else
            {
                maxTime = (double)h / (x + 1);
                minTime = (double)h / v;
            }
            Console.WriteLine("{0} {1}", minTime, maxTime);
        }
        static void Main(string[] args)
        {
            string[] input = Console.ReadLine().Split();
            int h = Convert.ToInt32(input[0]);
            int t = Convert.ToInt32(input[1]);
            int v = Convert.ToInt32(input[2]);
            int x = Convert.ToInt32(input[3]);
            Plane(h, t, v, x);
        }
    }
}
