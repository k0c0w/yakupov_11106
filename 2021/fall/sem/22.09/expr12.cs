﻿using System;

namespace sem
{
    class Class1
    {
        static void Plane(double h,double t, double v, double x)
        {
            double maxTime;
            double minTime;
            if (h / t > x)
            {
                maxTime = t;
                minTime = (h-x*t)/(v-x);
            }
            else
            {
                maxTime = h / (x + Double.Epsilon);
                minTime = 0;
            }
            Console.WriteLine("{0} {1}", minTime, maxTime);
        }
        static void Main(string[] args)
        {
            string[] input = Console.ReadLine().Split();
            double h = Convert.ToDouble(input[0]);
            double t = Convert.ToDouble(input[1]);
            double v = Convert.ToDouble(input[2]);
            double x = Convert.ToDouble(input[3]);
            Plane(h, t, v, x);
        }
    }
}
