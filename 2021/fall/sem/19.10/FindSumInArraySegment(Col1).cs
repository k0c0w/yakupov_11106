﻿using System;

namespace TestSpace
{
    class Program
    {
        static void Main(string[] args)
        {
            var array = new int[5] {0,1,2,3,4};
            var processedArray = ProcessArray(array);
            FindSumOnSegment(array,3);
        }

        static int[] ProcessArray(int[] array)
        {
            var sum = new int[array.Length];
            sum[0] = array[0];
            for (int i = 1; i < array.Length; i++)
            {
                sum[i] = sum[i - 1] + array[i];
            }
            return sum;
        }
        static void FindSumOnSegment(int[] array,int inputCount)
        {
            int i = 0;
            while (i<inputCount)
            {
                int segmentStart = int.Parse(Console.ReadLine());
                int segmentStop = int.Parse(Console.ReadLine());
                Console.WriteLine(array[segmentStop] - array[segmentStart]);
                i++;
            }
        }
    }
}