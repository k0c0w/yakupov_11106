﻿using System;

namespace TestSpace
{
    class Program
    {
        static void Main(string[] args)
        {
            var array = new int[5] {0,0,0,0,0};
            ProcessArray(array);
            foreach (var item in array)
                Console.Write("{0,-5}", item);
        }

        static void ProcessArray(int[] array)
        {
            GetUserData(array);
            for (int i = 1; i < array.Length; i++)
            {
                array[i] += array[i - 1];
            }
        }
        static void GetUserData(int[] array)
        {
            string input = Console.ReadLine();
            while (!string.IsNullOrEmpty(input))
            {
                var data = input.Split();
                var l = int.Parse(data[0]);
                var r = int.Parse(data[1]);
                var x = int.Parse(data[2]);
                if (l >= 0)
                    array[l] += x;
                else
                    array[0] += x;
                if (r < array.Length - 1)
                    array[r + 1] += -x;
                input = Console.ReadLine();
            }
        }
    }
}