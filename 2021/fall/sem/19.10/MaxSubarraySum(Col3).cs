﻿using System;

namespace TestSpace
{
    class Program
    {
        static void Main(string[] args)
        {
            var array = new int[12] { -1, 2, -8 ,5 , 5, 6, -1, 10, -100, 0,11,2,};
            FindMaxSubarray(array);
        }

        static void FindMaxSubarray(int[] array)
        {
            int result = array[0];
            int minSum = 0;
            int sum = 0;
            int startIndex = 0;
            int endIndex = 0;
            int minpos = -1;
            for (int i = 0; i < array.Length; i++)
            {
                sum += array[i];
                int currentSum = sum - minSum;
                if (currentSum > result)
                {
                    result = currentSum;
                    startIndex = minpos + 1;
                    endIndex = i;
                }
                if (sum <= minSum)
                {
                    minSum = sum;
                    minpos = i;
                }
            }
            Console.WriteLine("max sum {0} on [{1};{2}] of array", result, startIndex, endIndex);
        }
    }
}