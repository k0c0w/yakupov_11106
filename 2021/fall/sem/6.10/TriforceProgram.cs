﻿using System;

namespace TestSpace
{
    class JPlacedDigitInN
    {
        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            DrawTriforce(5);
        }
        static void DrawTriforce(int height)
        {
            for (int j = 1; j <= height; j++)
            {
                for (int i = 0; i < 2 * height  - j; i++)
                    Console.Write(' ');
                DrawStarPattern(j);
                Console.WriteLine();
            }
            Console.WriteLine();
            for (int j = 1; j <=height; j++)
            {
                for (int i = 0; i < height - j; i++)
                    Console.Write(' ');
                DrawStarPattern(j);
                for (int i = 0; i < 2 * height - 2*j + 1;i++)
                    Console.Write(' ');
                DrawStarPattern(j);
                Console.WriteLine();
            }
        }
        
        static void DrawStarPattern(int j)
        {
            for (int i = 0; i < 2 * j - 1; i++)
                Console.Write('*');
        }
    }
}
