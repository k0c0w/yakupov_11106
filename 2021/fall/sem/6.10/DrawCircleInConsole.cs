﻿using System;

namespace TestSpace
{
    class CircleInConsole
	{
        static void Main(string[] args)
        {
            DrawCircle(10);
        }    
        static void DrawCircle(int r)
        {
            for (int y = 0; y< 2*r +1; y++)
            {
                for (int x =0; x<2*r+1; x++)
                {
                    bool condition = (r - x) * (r - x) + (r - y) * (r - y) <= r * r;
                    if (condition)
                        Console.Write('0');
                    else
                        Console.Write('*');
                }
                Console.WriteLine();
            }
        }
    }
}
