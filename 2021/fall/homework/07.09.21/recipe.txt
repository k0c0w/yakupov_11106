Подготовьте необходимые продукты. Творог тщательно разомните, 
чтобы он был с минимальным количеством комочков.
Творог	500 г
Куриное яйцо	1 шт. = 60 г
Пшеничная мука хлебопекарная	5 ст. л. = 125 г
Сахар	2 ст. л. = 50 г
Разрыхлитель теста	0.5 ч. л. = 2 г
Растительное масло	3 ст. л. = 51 г

Возьмите глубокую миску. К растертому творогу добавьте сахар, соль и яйцо. 
Хорошо все перемешайте. Потом постепенно добавляйте муку. 
В зависимости от творога может уйти больше или меньше муки, 
следите за консистенцией. Тесто должно быть мягким, но при этом не прилипать к рукам.
Посыпьте стол мукой и скатайте из получившейся массы колбаску диаметром примерно 5-6 см. 
Разрежьте колбаску поперек на кружочки толщиной 1-2 см. Обваляйте их в муке.
Возьмите сковороду и налейте в нее масло, поставьте на средний огонь.
 Подождите, пока масло разогреется и начинайте выкладывать сырники. 
Обжаривайте их с каждой стороны по 3-4 минуты. 
Следите за тем, чтобы сырники не подгорали и оставались немного золотистого цвета.
Выложите готовые сырники пирамидкой. Сверху полейте вареньем и сметаной.