﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlWork3
{
    public class Point
    {
        public double X { get; private set; }
        public double Y { get; private set; }

        public Point(double x, double y)
        {
            this.X = x;
            this.Y = y;
        }

        public override string ToString()
        {
            return string.Format("({0};{1})", this.X, this.Y);
        }

        public override bool Equals(object obj)
        {
            if(obj is Point)
            {
                Point other = (Point)obj;
                return Math.Abs(this.X - other.X) <= Double.Epsilon && Math.Abs(this.Y - other.Y) <= Double.Epsilon; 
            }
            return base.Equals(obj);
        }
    }
}
