﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlWork3
{
    public class Segment 
    {
        public Point A { get; private set; }
        public Point B { get; private set; }

        private double lenght;

        public double Lenght
        {
            get { return lenght; }
        }

        public bool IsCrossing(Segment other)
        {
            var crossPoint = GetCrossingPoint(this, other);
            double x = crossPoint.X;
            double y = crossPoint.Y;
            bool xIsCorrect = (this.A.X <= x && x <= this.B.X 
                || this.A.X >= x && x >= this.B.X)
                && (other.A.X <= x && x <= other.B.X
                || other.A.X >= x &&  x >= other.B.X);
            bool yIsCorrect = (this.A.Y <= y && y <= this.B.Y
                || this.A.Y >= y && y >= this.B.Y)
                && (other.A.Y <= y && y <= other.B.Y
                || other.A.Y >= y && y >= other.B.Y);
            return xIsCorrect && yIsCorrect;
        }

        public double GetRectangleArea(double angleToSide)
        {
            return lenght * lenght * Math.Sin(angleToSide * 2) / 2;
        }
        public void ChangeA(Point newA)
        {
            this.A = newA;
            UpdateLenght();
        }

        public void ChangeB(Point newB)
        {
            this.B = newB;
            UpdateLenght();
        }

        public override string ToString()
        {
            return string.Format("Points : {0} and {1}; Lenght:{3}", this.A, this.B, lenght);
        }

        public override bool Equals(object obj)
        {
            if(obj is Segment)
            {
                Segment other = (Segment)obj;
                return this.A.Equals(other.A) && this.B.Equals(other.B) || this.B.Equals(other.A) && this.A.Equals(other.B);
            }
            return base.Equals(obj);
        }

        public Segment(Point a, Point b)
        {
            this.A = a;
            this.B = b;
            UpdateLenght();
        }

        public Segment()
        {
            this.A = new Point(0,0);
            this.B = new Point(0, 0);
            UpdateLenght();
        }

        private void UpdateLenght()
        {
            lenght = GetLenght(this.A, this.B);
        }

        private double GetLenght(Point A, Point B)
        {
            return Math.Sqrt((A.X - B.X) * (A.X - B.X) + (A.Y - B.Y) * (A.Y - B.Y));
        }

        private Point GetCrossingPoint(Segment a, Segment b)
        {
            bool isVertical1 = false;
            bool isVertical2 = false;
            try
            {
                double k1 = (a.A.Y - a.B.Y) / (a.A.X - a.B.X);
                double b1 = a.A.Y - k1 * a.A.X;
            }
            catch (DivideByZeroException)
            {  isVertical1 = true; }

            try
            {
                double k2 = (b.A.Y - b.B.Y) / (b.A.X - b.B.X);
                double b2 = b.A.Y - k2 * b.A.X;
            }
            catch (DivideByZeroException)
            {   isVertical2 = true; }

            if(isVertical1 && !isVertical2)
            {
                if((b.A.X <= a.A.X && a.A.X <= b.B.X) || (b.B.X <= a.A.X && a.A.X <= b.A.X))
                        return new Point()
            }
            return new Point(x3, y3);
        }
    }
}
