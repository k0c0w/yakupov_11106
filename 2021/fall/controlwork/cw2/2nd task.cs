using System;

namespace ControlWork.Ex2
{
    class Program 
    {
        static void Main()
        {
            var numbers = GetArray();
            Console.WriteLine(ContainsElem(numbers));
        }

        static bool ContainsElem(int[] array)
        {
            if (array == null || array.Length == 0)
                return false;
            return ContainsElem(array, 0);
        }

        static bool ContainsElem(int[] array, int position)
        {
            if (position == array.Length)
                return false;
            //array starts with 0, but elem is first
            if (position % 5 == 4 && Math.Abs(array[position]) % 2 == 0)
                return true;
            return ContainsElem(array, position + 1);
        }

        static int[] GetArray()
        {
            var entry = Console.ReadLine();
            if (entry.Length == 0)
                return null;
            var input = entry.Split();
            int[] resultArray = new int[input.Length];
            for(int i =0; i < resultArray.Length; i++ )
            {
                resultArray[i] = int.Parse(input[i]);
            }
            return resultArray;
        }
    }
}