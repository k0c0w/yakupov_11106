using System;

namespace ControlWork.Ex1
{
    class Program
    {
        static void Main()
        {

            var line = MixLettersRegister(GetString());
            PrintString(line);
        }

        static void PrintString(string line)
        {
            Console.WriteLine(line);
        }

        static string GetString()
        {
            return Console.ReadLine();
        }

        static string MixLettersRegister(string line)
        {
            if (line == null)
                return null;
            var resultLine = new System.Text.StringBuilder();
            int letterCount = 0;
            foreach(var symbol in line)
            {
                if (char.IsLetter(symbol))
                {
                    if (letterCount % 2 == 0)
                        resultLine.Append(char.ToUpper(symbol));
                    else
                        resultLine.Append(char.ToLower(symbol));
                    ++letterCount;
                }
                else
                {
                    resultLine.Append(symbol);
                    ++letterCount;
                }
            }
            return resultLine.ToString();
        }
        /*
         IsNull - must return null
         StringWithOnlyLetters - input aBcd - output AbCd
        SpecialString - input a\nb output A\nB
        mixedString  - input a55embler output A55eMbLeR
        empty - return empty string
         */
    }
}