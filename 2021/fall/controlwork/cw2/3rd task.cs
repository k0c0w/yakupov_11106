using System;
using System.Collections.Generic;

namespace ControlWork.Ex2
{
    class Program 
    {
        static void Main()
        {
            var line = "asbsfg";
            int len = GetUniqueSubstringLength(line);
            Console.WriteLine(len);
        }

        static int GetUniqueSubstringLength(string line)
        {
            int maxLength = 0;
            var currentSymbols = new System.Text.StringBuilder();
            for(int i = 0; i < line.Length; i++)
            {
                int indexOfRepetition = Array.BinarySearch(currentSymbols.ToString().ToCharArray(), line[i]);//GetIndexOfElem(Sort(currentSymbols));
                if (indexOfRepetition >= 0)
                    currentSymbols.Remove(0, indexOfRepetition + 1);
                currentSymbols.Append(line[i]);
                maxLength = Math.Max(maxLength, currentSymbols.Length);
            }

            return maxLength;
        }

        static int GetIndexOfElem(string line, char elem)
        {
            int l = -1;
            int r = line.Length;
            
            while (true)
            {
                int index = (l + r) / 2;
                if (elem < line[index])
                    r = line[index] + 1;
                else
                    l = index;
               
            }
        }
        static string Sort()
        {
            //...
            return null;
        }
    }
}