﻿using System;

namespace ControlWork1
{
    class Program
    {
        static void Main(string[] args)
        {
            Test3();
        }
        
        static void Test1()
        {
            int n = int.Parse(Console.ReadLine());
            var numbers = GetArray(n);
            int count = 0;
            foreach(var item in numbers )
            {
                if (item % 3 == 0)
                    ++count;
            }
            Console.WriteLine(ContainsOnlyOdd(count));
        }

        static void Test2()
        {
            int n = int.Parse(Console.ReadLine());
            int[] a = GetArray(n);
            int count = 0;
            int iFactorial = 1;
            int currentTwoPow = 1;
            for (int i = 0; i<n; i++)
            {
                if (i != 0)
                {
                    iFactorial *= i;
                    currentTwoPow *= 2;
                }
                double b = currentTwoPow + iFactorial;
                double D = b*b + 4 * FindFactorial(a[i]);
                if (AnswerContainsEven(b, D))
                    ++count;
            }
            int sumOfCountDigits = FindSumOfDigits(count);

            Console.WriteLine(sumOfCountDigits);
        }

        static void Test3()
        {
            int m = int.Parse(Console.ReadLine());
            int n = int.Parse(Console.ReadLine());
            Get(m,n);
            //var array1 = GetArray(m);
            //var array2 = GetArray(n);

        }
        
        static void Get(int m, int n)
        {
            int a = 0;
            int b = 0;
            int c = 0;
            bool breakFlag = false;
            for (int i= 1; i<= (int)Math.Sqrt(m)+1; i++)
            {
                
                a = 0;
                b = 0;
                if (m % i == 0)
                {
                    a = i;
                    b = m / i;
                }
                for (int j = 1; j<= (int)Math.Sqrt(n) + 1; j++)
                {
                    c = 0;
                    if (n % j == 0)
                    {
                        c = n / j;
                    }
                    if (b * c == n)
                    {
                        breakFlag = true;
                        break;
                    }
                        
                }
                if (breakFlag)
                    break;
                Console.WriteLine("{0}=m {1}=n", a * b, b * c);
            }
        }
        static int FindSumOfDigits(int number)
        {
            int sum = 0;
            while (number != 0)
            {
                sum += number % 10;
                number /= 10;
            }
            return sum;
        }
        static bool AnswerContainsEven(double b, double D)
        {
           int x1;
           int x2;
            
            if (D > 0)
            {
                x1 = (int)((-b + Math.Sqrt(D)) / 2);
                x2 = (int)((-b - Math.Sqrt(D)) / 2);
                return ContainsEven(x1) || ContainsEven(x2);
            }
            else if (D == 0)
            {
                x1 = (int)((-b + Math.Sqrt(D)) / 2);
                return ContainsEven(x1);
            }

            return false;
        }

        static int FindFactorial(int n)
        {
            int factorial = 1;
            if (n != 0)
            {
                for (int i = 1; i <= n; i++)
                    factorial *= i;
            }
            return factorial;
        }
        static int[] GetArray(int arrayLenght)
        {
            int[] array = new int[arrayLenght];
            for (int i = 0; i < arrayLenght; i++)
            {
                array[i] = int.Parse(Console.ReadLine());
            }
            return array;
        }
        
        static bool ContainsEven(int number)
        {
            number = Math.Abs(number);
            while (number != 0)
            {
                if ((number % 10) % 2 == 0)
                    return true;
                number /= 10;
            }
            return false;
        }
        static bool ContainsOnlyOdd(int number)
        {
            while (number != 0)
            {
                if ((number % 10) % 2 == 0)
                    return false;
                number /= 10;
            }
            return true;
        }
    }
}