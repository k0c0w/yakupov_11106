﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms.DataVisualization.Charting;
using System.Windows.Forms;

namespace graphikSem
{
    internal  class GraphForm : Form
    {
        static Series iterationsOnList = new Series();
        static Series iterationsOnTree = new Series();
        static Series iterationsOnArray = new Series();
        static Series timeOnArray = new Series();
        static Series timeOnTree = new Series();
        static Series timeOnList = new Series();

        enum MeasureType
        {
            Iterations,
            TimeInSeconds
        }

        public static void PrepareDataForForms(string path)
        {
            var reader = new FileReader(path);
            for (int i = 100; i <= 10000; i += 100)
            {
                Console.WriteLine(i);

                var sortArray = Measure.MeasureTimeAndIterations(x => TreeSortOnArray<int>.Sort(x),
                    TreeSortOnArray<int>.GetIterationsCount, i, reader);
                var sortList = Measure.MeasureTimeAndIterations(x => TreeSortOnList<int>.Sort(x), 
                    TreeSortOnList<int>.GetIterationsCount, i, reader);
                var sortTree = Measure.MeasureTimeAndIterations(x => TreeSortOnTree<int>.Sort(x), 
                    TreeSortOnTree<int>.GetIterationsCount, i, reader);

                timeOnArray.Points.Add(new DataPoint(i, sortArray.Item1));
                timeOnTree.Points.Add(new DataPoint(i, sortTree.Item1));
                timeOnList.Points.Add(new DataPoint(i, sortList.Item1));

                iterationsOnArray.Points.Add(new DataPoint(i, sortArray.Item2));
                iterationsOnTree.Points.Add(new DataPoint(i, sortTree.Item2));
                iterationsOnList.Points.Add(new DataPoint(i, sortList.Item2));
            }
        }
        public static Form GetTimeGraphForm()
        {
            return GetForm(timeOnArray, timeOnList, timeOnTree, MeasureType.TimeInSeconds);
        }

        public static Form GetIterationsGraphForm()
        {
            return GetForm(iterationsOnArray, iterationsOnList, iterationsOnTree, MeasureType.Iterations);
        }

        private static Form GetForm(Series array, Series list, Series tree, MeasureType measureType)
        {
            var chart = new Chart();
            chart.ChartAreas.Add(new ChartArea());

            Axis ax = new Axis();
            ax.Title = "Input Size";
            chart.ChartAreas[0].AxisX = ax;
            Axis ay = new Axis();
            ay.Title = measureType.ToString();
            chart.ChartAreas[0].AxisY = ay;

            array.ChartType = SeriesChartType.FastLine;
            array.Color = Color.Red;
            array.MarkerBorderWidth = 3;
            array.Name = "TreeSort on array";

            list.ChartType = SeriesChartType.FastLine;
            list.Color = Color.Blue;
            list.BorderWidth = 3;
            list.Name = "TreeSort on List";

            tree.ChartType = SeriesChartType.FastLine;
            tree.Color = Color.Green;
            tree.MarkerBorderWidth = 3;
            tree.Name = "TreeSort on tree";

            chart.Series.Add(array);
            chart.Series.Add(list);
            chart.Series.Add(tree);
            chart.Legends.Add(new Legend());

            chart.Dock = System.Windows.Forms.DockStyle.Fill;
            var form = new Form();
            form.Controls.Add(chart);

            return form;
        }
    }
}
