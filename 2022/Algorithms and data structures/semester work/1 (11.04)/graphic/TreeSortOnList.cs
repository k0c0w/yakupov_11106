﻿using System;

namespace graphikSem
{
    public class TreeSortOnList<T> where T : IComparable<T>
    {
        //only for preparing data
        private static long Iterations;

        //(data, parrent, leftleaf, rightleaf)
        private MyList<(T, int, int, int)> heap;

        public static void Sort(T[] array)
        {
            if (array == null || array.Length == 0)
                return;
            var sorter = new TreeSortOnList<T>();
            foreach (var item in array)
            {
                sorter.Insert(item);
            }
            sorter.FillResult(array);
        }

        TreeSortOnList()
        {
            //only for preparing data
            Iterations = 0;
            heap = new MyList<(T, int, int, int)>();
        }

        private void Insert(T item)
        {
            if (heap.Count == 0)
            {
                heap.Add((item, -1, -1, -1));
                return;
            }
            int index = 0;
            bool lessThanParrent = true;
            while (true)
            {
                //only for testing
                Iterations++;

                int parrent = index;
                var currentTuple = heap[index];
                if (item.CompareTo(currentTuple.Item1) < 0)
                {
                    index = currentTuple.Item3;
                    lessThanParrent = true;
                }
                else
                {
                    index = currentTuple.Item4;
                    lessThanParrent = false;
                }
                if (index == -1)
                {
                    AddToFolder(item, parrent, lessThanParrent);
                    break;
                }
            }
        }

        private void AddToFolder(T item, int parrent, bool lessThanParrent)
        {
            var currentTuple = heap[parrent];
            if (lessThanParrent)
            {

                heap[parrent] = (currentTuple.Item1, currentTuple.Item2, heap.Count, currentTuple.Item4);
            }
            else
            {
                heap[parrent] = (currentTuple.Item1, currentTuple.Item2, currentTuple.Item3, heap.Count);
            }
            heap.Add((item, parrent, -1, -1));
        }
        // I had StackOverFlow thats why there is cycle way of Traversing.
        private void FillResult(T[] array)
        {
            // roots that we have visited (if visited then true)
            bool[] visited = new bool[array.Length];
            var current = heap[0];
            //if root was comebacking up to parrent from all iterations
            bool returned = false;
            int tupleIndex = 0;
            int index = 0;
            while (index < array.Length)
            {
                Iterations++;
                if (current.Item3 != -1 && !returned)
                {
                    tupleIndex = current.Item3;
                    current = heap[tupleIndex];
                    returned = false;
                    continue;
                }
                if (!visited[tupleIndex])
                {
                    visited[tupleIndex] = true;
                    array[index] = current.Item1;
                    index++;
                    returned = false;
                }

                if (current.Item4 != -1 && !returned)
                {
                    tupleIndex = current.Item4;
                    current = heap[tupleIndex];
                    returned = false;
                    continue;
                }
                tupleIndex = current.Item2;
                if (tupleIndex != -1)
                    current = heap[tupleIndex];
                returned = true;
            }
        }
        /// <summary>
        /// Returns static Iterations Count for preparing data
        /// </summary>
        /// <returns></returns>
        public static long GetIterationsCount()
        {
            return Iterations + MyList<T>.GetIterationsCount();
        }
    }
}
