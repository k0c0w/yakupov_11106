﻿
namespace SemestrWork
{
    internal class Program
    {
        static void Main()
        {
            Console.WriteLine("B-Tree");
            var bTree = new BTree<int>(3);
            bTree.Insert(2);
            bTree.Insert(0);
            bTree.Insert(10);
            bTree.Insert(115);
            bTree.Insert(2);
            bTree.Insert(26);
            bTree.Insert(4);
            bTree.Traverse(x => Console.Write(x + " "));
            bTree.Insert(1307);
            bTree.Insert(228);
            Console.WriteLine();
            Console.WriteLine("228 has been inserted. is it in? " + bTree.Contains(228));
            bTree.Remove(2);
            bTree.Remove(2);
            Console.WriteLine("2 has been double removed. is it in? " + bTree.Contains(2));
            bTree.Traverse(x => Console.Write(x + " "));
            Console.WriteLine();

            Console.WriteLine("B+Tree");
            var bPlusTree = new BPlusTree<int>(3);
            bPlusTree.Insert(2, 2);
            bPlusTree.Insert(0, 0);
            bPlusTree.Insert(10, 10);
            bPlusTree.Insert(115, 115);
            bPlusTree.Insert(2, 2);
            bPlusTree.Insert(26, 26);
            bPlusTree.Insert(4, 4);
            bPlusTree.Traverse(x => Console.Write(x + " "));
            bPlusTree.Insert(228, 228);
            bPlusTree.Insert(1307, 1307);
            Console.WriteLine();
            Console.WriteLine("228 has been inserted. is it in? " + bPlusTree.Contains(228));
            bPlusTree.Traverse(x => Console.Write(x + " "));
            Console.WriteLine();
        }
    }
}
