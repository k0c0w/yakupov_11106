﻿
namespace SemestrWork
{
    public class BTree<TData>  where TData : IComparable<TData>
    {
        BTreeNode? root;
        int TreeMinDegree { get; init; }

        public bool IsEmpty => root == null;

        public BTree(int minimalDegree)
        {
            TreeMinDegree = minimalDegree;
        }

        /// <summary>
        /// Traverses Tree and complete <paramref name="action"/> with presented keys.
        /// </summary>
        /// <param name="action"></param>
        public void Traverse(Action<TData> action)
        {
            if (root != null)
                root.Traverse(action);
        }
        
        /// <summary>
        /// Check if Tree contains <paramref name="item"/>.
        /// </summary>
        /// <param name="item"></param>
        /// <returns>
        /// True if presented, else - False
        /// </returns>
        public bool Contains(TData item)
        {
            return SearchKeyInNodes(item) != null;
        }

        /// <summary>
        /// Inserts a new <paramref name="key"/> in tree.
        /// </summary>
        /// <param name="key"></param>
        public void Insert(TData key)
        {
            if (root == null)
            {
                root = new BTreeNode(true, this);
                root.Keys[0] = key;
                root.KeysCount = 1;
            }
            else
            {
                if (root.KeysCount == 2 * TreeMinDegree - 1)
                {
                    var node = new BTreeNode(false, this);
                    node.Childrens[0] = root;
                    node.Split(0, root);
                    int i = 0;
                    if (node.Keys[0].CompareTo(key) < 0)
                        i++;
                    node.Childrens[i].InsertNotFull(key);

                    root = node;
                }
                else
                    root.InsertNotFull(key);
            }
        }

        /// <summary>
        /// Removes <paramref name="key"/> from tree.
        /// </summary>
        /// <param name="key"></param>
        /// <exception cref="InvalidOperationException"></exception>
        public void Remove(TData key)
        {
            if (root == null)
                throw new InvalidOperationException(string.Format("{0} is empty!", this));

            root.Remove(key);

            //rebalancing
            if (root.KeysCount == 0)
            { 
                if (root.IsLeaf)
                    root = null;
                else
                    root = root.Childrens[0];
            }
        }
        
        /// <summary>
        /// Searches for <paramref name="key"/> in tree.
        /// </summary>
        /// <param name="key"></param>
        /// <returns>
        /// BTreeNode wich contains <paramref name="key"/> or null.
        /// </returns>
        private BTreeNode? SearchKeyInNodes(TData key)
        {
            return root?.SearchKey(key);
        }

        class BTreeNode
        {
            readonly BTree<TData> _globalTree;
            public readonly TData[] Keys;
            public readonly BTreeNode[] Childrens;

            /// <summary>
            /// Current count of keys in node.
            /// </summary>
            public int KeysCount { get; set; }
            public bool IsLeaf { get; set; }

            public BTreeNode(bool isLeaf, BTree<TData> tree)
            {
                _globalTree = tree;
                IsLeaf = isLeaf;
                Keys = new TData[2 * tree.TreeMinDegree - 1];
                Childrens = new BTreeNode[2 * tree.TreeMinDegree];
                KeysCount = 0;
            }

            /// <summary>
            /// Traverses a subtree rooted with this node and completes <paramref name="action"/> with keys.
            /// </summary>
            /// <param name="action"></param>
            public void Traverse(Action<TData> action)
            {
                int i;
                for (i = 0; i < KeysCount; i++)
                {
                    if (!IsLeaf)
                        Childrens[i].Traverse(action);
                    action(Keys[i]);
                }

                if (!IsLeaf)
                    Childrens[i].Traverse(action);
            }

            /// <summary>
            /// Searches node where <paramref name="key"/> is presented.
            /// </summary>
            /// <param name="key"></param>
            /// <returns>
            /// BTreeNode or null.
            /// </returns>
            public BTreeNode? SearchKey(TData key)
            {
                int i = 0;
                while (i < KeysCount && key.CompareTo(Keys[i]) > 0)
                    i++;

                if (Keys[i].Equals(key))
                    return this;

                if (IsLeaf)
                    return null;
                return Childrens[i].SearchKey(key);
            }

            /// <summary>
            /// Inserts a new <paramref name="key"/> in the subtree rooted with this node.
            /// The node must be not full when operation is called
            /// </summary>
            /// <param name="key"></param>
            public void InsertNotFull(TData key)
            {
                var i = KeysCount - 1;
                if (IsLeaf)
                {
                    while (i >= 0 && Keys[i].CompareTo(key) > 0)
                    {
                        Keys[i + 1] = Keys[i];
                        i--;
                    }
                    Keys[i + 1] = key;
                    KeysCount++;
                }
                else
                {
                    while (i >= 0 && Keys[i].CompareTo(key) > 0)
                        i--;
                    if (Childrens[i + 1].KeysCount == 2 * _globalTree.TreeMinDegree - 1)
                    {
                        Split(i + 1, Childrens[i + 1]);
                        if (Keys[i + 1].CompareTo(key) < 0)
                            i++;
                    }
                    Childrens[i + 1].InsertNotFull(key);
                }
            }

            /// <summary>
            /// Divides child node <paramref name="splitable"/> of this node.
            /// <paramref name="splitable"/> must be full when Split() is called.
            /// </summary>
            /// <param name="i"> index of <paramref name="splitable"/> in node`s Childrens </param>
            /// <param name="splitable"></param>
            public void Split(int i, BTreeNode splitable)
            {
                var created = new BTreeNode(splitable.IsLeaf, splitable._globalTree)
                {
                    KeysCount = _globalTree.TreeMinDegree - 1
                };
                for (int j = 0; j < _globalTree.TreeMinDegree - 1; j++)
                    created.Keys[j] = splitable.Keys[j + _globalTree.TreeMinDegree];
                if (!splitable.IsLeaf)
                    for (int j = 0; j < _globalTree.TreeMinDegree; j++)
                        created.Childrens[j] = splitable.Childrens[j + _globalTree.TreeMinDegree];

                splitable.KeysCount = _globalTree.TreeMinDegree - 1;

                for (int j = KeysCount; j >= i + 1; j--)
                    Childrens[j + 1] = Childrens[j];
                Childrens[i + 1] = created;

                for (int j = KeysCount - 1; j >= i; j--)
                    Keys[j + 1] = Keys[j];
                Keys[i] = splitable.Keys[_globalTree.TreeMinDegree - 1];

                KeysCount++;
            }

            // RemoveFromLeaf, RemoveFromNonLeaf, GetPred, GetSucc, Borrow..., Merge, FindKey functions are used only for delete key from node
            /// <summary>
            /// Removes <paramref name="key"/> from subtree rooted with this node.
            /// Throws exeption if <paramref name="key"/> is not presented.
            /// </summary>
            /// <param name="key"></param>
            /// <exception cref="InvalidOperationException"></exception>
            public void Remove(TData key)
            {

                int index = FindKey(key);
                if (index < KeysCount && Keys[index].Equals(key))
                {
                    if (IsLeaf)
                        RemoveFromLeaf(index);
                    else
                        RemoveFromNonLeaf(index);
                }
                else
                {
                    if (IsLeaf)
                        throw new InvalidOperationException(string.Format("{0} is not presented in {1}", key, _globalTree));
                    var existsInChildren = index == KeysCount;

                    if (Childrens[index].KeysCount < _globalTree.TreeMinDegree)
                        Fill(index);

                    if (existsInChildren && index > KeysCount)
                        Childrens[index - 1].Remove(key);
                    else
                        Childrens[index].Remove(key);
                }
            }

            /// <summary>
            /// </summary>
            /// <param name="key"></param>
            /// <returns>
            /// The index of the first key that is greater  or equal to <paramref name="key"/>.
            ///</returns>
            private int FindKey(TData key)
            {
                int index = 0;
                while (index < KeysCount && Keys[index].CompareTo(key) < 0)
                    ++index;
                return index;
            }

            /// <summary>
            /// remove the key present in <paramref name="index"/> position on this node which is a leaf.
            /// </summary>
            /// <param name="index"></param>
            private void RemoveFromLeaf(int index)
            {
                for (int i = index + 1; i < KeysCount; ++i)
                    Keys[i - 1] = Keys[i];
                KeysCount--;
            }

            /// <summary>
            /// remove the key present on <paramref name="index"/> position in this node which is not leaf.
            /// </summary>
            /// <param name="index"></param>
            private void RemoveFromNonLeaf(int index)
            {
                var key = Keys[index];
                if (Childrens[index].KeysCount >= _globalTree.TreeMinDegree)
                {
                    var pred = GetPredecessor(index);
                    Keys[index] = pred;
                    Childrens[index].Remove(pred);
                }
                else if (Childrens[index + 1].KeysCount >= _globalTree.TreeMinDegree)
                {
                    var succ = GetSuccessor(index);
                    Keys[index] = succ;
                    Childrens[index + 1].Remove(succ);
                }
                else
                {
                    Merge(index);
                    Childrens[index].Remove(key);
                }
            }

            /// <summary>
            /// </summary>
            /// <param name="index"></param>
            /// <returns>
            /// Gets the predecessor of the key- where the key is present 
            /// on the <paramref name="index"/> position in the node.
            /// </returns>
            private TData GetPredecessor(int index)
            {
                var current = Childrens[index];
                while (!current.IsLeaf)
                    current = current.Childrens[current.KeysCount];
                return current.Keys[current.KeysCount - 1];
            }

            /// <summary>
            /// </summary>
            /// <param name="index"></param>
            /// <returns>
            /// Gets the successor of the key- where the key is present 
            /// on the <paramref name="index"/> position in the node
            /// </returns>
            private TData GetSuccessor(int index)
            {
                var current = Childrens[index + 1];
                while (!current.IsLeaf)
                    current = current.Childrens[0];
                return current.Keys[0];
            }

            /// <summary>
            /// Connects <paramref name="index"/> of the node with next node`s child
            /// </summary>
            /// <param name="index"></param>
            private void Merge(int index)
            {
                var child = Childrens[index];
                var neighbor = Childrens[index + 1];
                child.Keys[_globalTree.TreeMinDegree - 1] = Keys[index];
                for (int i = 0; i < neighbor.KeysCount; ++i)
                    child.Keys[i + _globalTree.TreeMinDegree] = neighbor.Keys[i];

                if (!child.IsLeaf)
                    for (int i = 0; i <= neighbor.KeysCount; ++i)
                        child.Childrens[i + _globalTree.TreeMinDegree] = neighbor.Childrens[i];

                for (int i = index + 1; i < KeysCount; ++i)
                    Keys[i - 1] = Keys[i];

                for (int i = index + 2; i <= KeysCount; ++i)
                    Childrens[i - 1] = Childrens[i];

                child.KeysCount += neighbor.KeysCount + 1;
                KeysCount--;
            }

            /// <summary>
            /// Fills up the node`s child presented on <paramref name="index"/>
            /// if that child has less than TreeMinDegree - 1 keys.
            /// </summary>
            /// <param name="index"></param>
            private void Fill(int index)
            {
                if (index != 0 && Childrens[index - 1].KeysCount >= _globalTree.TreeMinDegree)
                    BorrowFromPreviousNode(index);
                else if (index != KeysCount && Childrens[index + 1].KeysCount >= _globalTree.TreeMinDegree)
                    BorrowFromNextNode(index);
                else
                {
                    if (index != KeysCount)
                        Merge(index);
                    else
                        Merge(index - 1);
                }
            }

            /// <summary>
            /// Borrow a key from the node`s children on <paramref name="index"/> - 1 and placeit on <paramref name="index"/> node.
            /// </summary>
            /// <param name="index"></param>
            private void BorrowFromPreviousNode(int index)
            {
                var child = Childrens[index];
                var neighbor = Childrens[index - 1];

                for (int i = child.KeysCount - 1; i >= 0; --i)
                    child.Keys[i + 1] = child.Keys[i];

                if (!child.IsLeaf)
                    for (int i = child.KeysCount; i >= 0; --i)
                        child.Childrens[i + 1] = child.Childrens[i];

                child.Keys[0] = Keys[index - 1];
                if (!child.IsLeaf)
                    child.Childrens[0] = neighbor.Childrens[neighbor.KeysCount];

                Keys[index - 1] = neighbor.Keys[neighbor.KeysCount - 1];
                child.KeysCount += 1;
                neighbor.KeysCount -= 1;
            }

            /// <summary>
            /// Borrow a key from the node`s children on <paramref name="index"/> + 1 and placeit on <paramref name="index"/> node.
            /// </summary>
            /// <param name="index"></param>
            private void BorrowFromNextNode(int index)
            {
                var child = Childrens[index];
                var neighbor = Childrens[index + 1];
                child.Keys[child.KeysCount] = Keys[index];

                if (!child.IsLeaf)
                    child.Childrens[child.KeysCount + 1] = neighbor.Childrens[0];

                Keys[index] = neighbor.Keys[0];

                for (int i = 1; i < neighbor.KeysCount; ++i)
                    neighbor.Keys[i - 1] = neighbor.Keys[i];

                if (!neighbor.IsLeaf)
                    for (int i = 1; i <= neighbor.KeysCount; ++i)
                        neighbor.Childrens[i - 1] = neighbor.Childrens[i];
                child.KeysCount++;
                neighbor.KeysCount--;
            }
        }
    }
}