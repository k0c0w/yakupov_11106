﻿using System;
using System.Collections.Generic;

namespace Cyberpunk_2088
{
    public class Graph
    {
        int[] binding;
        int[] treeSize;

        public Graph(int numberOfPeaks)
        {
            binding = new int[numberOfPeaks];
            treeSize = new int[numberOfPeaks];
            Initialize(numberOfPeaks);
        }
        private void Initialize(int n)
        {
            for(int i = 0; i < n; i++)
            {
                binding[i] = i;
                treeSize[i] = 1;
            }
        }
        public void Add(int p, int q)
        {
            int peak1 = FindRoot(p);
            int peak2 = FindRoot(q);
            if (peak1 != peak2)
                Union(peak1, peak2);
        }
        public void PrintAllBindings()
        {
            for(int i= 0; i < binding.Length; i++)
            {
                Console.WriteLine("{0} - {1}", i, FindRoot(binding[i]));
            }
        }
        private int FindRoot(int peak)
        {
            while(peak != binding[peak])
            {
                binding[peak] = binding[binding[peak]];
                peak = binding[peak];
            }
                peak = binding[peak];
            return peak;
        }

        private void Union(int peak1, int peak2)
        {
            if (treeSize[peak1] < treeSize[peak2])
            {
                binding[peak1] = peak2;
                treeSize[peak2] += treeSize[peak1];
            }
            else
            {
                binding[peak2] = peak1;
                treeSize[peak1] += treeSize[peak2];
            }
        }
    }
}
