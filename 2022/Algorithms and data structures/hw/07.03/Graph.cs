﻿using System;
using System.Collections.Generic;

namespace Cyberpunk_2088
{
    public class Graph<TValue>
    {
        Dictionary<TValue, TValue> binding;
        Dictionary<TValue,int> treeSize;

        public Graph()
        {
            binding = new Dictionary<TValue, TValue>();
            treeSize = new Dictionary<TValue, int>();
        }

        public void Add(TValue A, TValue B)
        {
            if(A == null || B == null) throw new ArgumentNullException("One of peaks is null");
            if (!binding.ContainsKey(A))
            {
                binding.Add(A, A);
                treeSize.Add(A, 1);
            }
            if(!binding.ContainsKey(B))
            {
                binding.Add(B, B);
                treeSize.Add(B, 1);
            }  
            TValue peak1 = FindRoot(A);
            TValue peak2 = FindRoot(B);
            if (!peak1.Equals(peak2))
                Union(peak1, peak2);
        }
        public List<Tuple<TValue,TValue>> GetAllBindings()
        {
            var result = new List<Tuple<TValue,TValue>>();
            foreach(var key in binding.Keys)
            {
                result.Add(Tuple.Create(key, FindRoot(key)));
            }
            return result;
        }

        private TValue FindRoot(TValue peak)
        {
            while(!peak.Equals(binding[peak]))
            {
                binding[peak] = binding[binding[peak]];
                peak = binding[peak];
            }

            return peak;
        }

        private void Union(TValue peak1, TValue peak2)
        {
            if (treeSize[peak1] < treeSize[peak2])
            {
                binding[peak1] = peak2;
                treeSize[peak2] += treeSize[peak1];
            }
            else
            {
                binding[peak2] = peak1;
                treeSize[peak1] += treeSize[peak2];
            }
        }
    }
}