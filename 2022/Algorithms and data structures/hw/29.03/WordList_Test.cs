using NUnit.Framework;
using List.WordList;
using System.Collections.Generic;

namespace TestProject1
{ 
    [TestFixture]
    public class WordList_Test
    {
        [Test]
        public void Create()
        {
            new WordList_Test();
        }
        
        [Test, TestCaseSource("AddCases")]
        public void Add_Test(List<string> items, List<string> expected)
        {
            var list = CreateFrom(items);
            var result = list.GetWords();

            Assert.That(result, Is.EqualTo(expected));
        }

        [Test, TestCaseSource("RemoveCases")]
        public void Remove_Test(List<string> fill, List<string> remove, List<string> expected)
        {
            var list = CreateFrom(fill);

            foreach(var word in remove)
            { 
                list.Remove(word);
            }
            var result = list.GetWords();

            Assert.That(result, Is.EqualTo(expected));
        }

        [Test, TestCaseSource("LinkCases")]
        public void Link_Test(List<string> source1, List<string> source2, List<string> expected)
        {
            var l1 = CreateFrom(source1);
            var l2 = CreateFrom(source2);

            l1.Link(l2);
            var result = l1.GetWords();

            Assert.That(result, Is.EqualTo(expected));
        }

        [Test, TestCaseSource("SplitCases")]
        public void SplitByVowelsAndConsonants_Test(List<string> source1, List<string> expectedVowels, List<string> expectedConsonants)
        {
            var l = CreateFrom(source1);

            var tuple = l.SplitByVowelsAndConsonants();
            var result1 = tuple.Item1.GetWords();
            var result2 = tuple.Item2.GetWords();
            
            Assert.That(result1, Is.EqualTo(expectedVowels));
            Assert.That(result2, Is.EqualTo(expectedConsonants));
        }

        [Test, TestCaseSource("PalindromsCases")]
        public void RemovePalindroms_Test(List<string> items, List<string> expected)
        {
            var list = CreateFrom(items);

            list.RemovePalindroms();
            var result = list.GetWords();

            Assert.That(result, Is.EqualTo(expected));
        }

        [Test, TestCaseSource("LengthCases")]
        public void WithWordLength_Test(List<string> items, int length, List<string> expected)
        {
            var list = CreateFrom(items);

            list = list.WithWordsLength(length);
            var result = list.GetWords();

            Assert.That(result, Is.EqualTo(expected));
        }

        private List.WordList.WordList CreateFrom(List<string> source)
        {
            var list = new List.WordList.WordList();

            foreach (var word in source)
            {
                list.Add(word);
            }
            return list;
        }
        #region
        private static readonly object[] AddCases =
        {
        new [] {new List<string> { "������", "1", "hola" }, new List<string> { "������", "1", "hola" } },
        new [] {new List<string> { "������", "", "hola" }, new List<string> { "������", "hola" } },
        new [] {new List<string> { "������", "hola", "������" }, new List<string> { "������", "hola" } }
        };

        private static readonly object[] RemoveCases =
        {
        new [] {new List<string> { "������", "1", "hola", "2", "3" }, new List<string> { "������", "1", "2" } , new List<string> {"hola", "3"} },
        new [] {new List<string> { "������", "hola" }, new List<string> {"5"} ,new List<string> { "������", "hola" } },
        new [] {new List<string> { "������", "hola" }, new List<string> {"", null!}, new List<string> { "������", "hola" } }
        };

        private static readonly object[] LinkCases =
        {
        new [] {new List<string> { "1", "2", "3"}, new List<string> { "4", "5" } , new List<string> {"1", "2", "3", "4", "5"} },
        new [] {new List<string>(), new List<string> {"5", "4"} ,new List<string> { "5", "4" } },
        };

        private static readonly object[] SplitCases =
        {
        new [] {new List<string> { "apple", "juice", "banana", "arrow"}, new List<string> { "apple", "arrow" } , new List<string> {"juice", "banana"} },
        new [] {new List<string> { "apple", "juice", "1"}, new List<string> { "apple"} , new List<string> {"juice"} },
        new [] {new List<string> {"apple", "alien",""}, new List<string> { "apple", "alien"} , new List<string>() },
        };

        private static readonly object[] PalindromsCases =
        {
        new [] {new List<string> { "tenet", "mom", "deed" }, new List<string>()},
        new [] {new List<string> { "tenet", "apple", "1", "lol"}, new List<string> { "apple"} },
        new [] {new List<string> {"apple", "alien","deed", ""}, new List<string> { "apple", "alien"} },
        };

        private static readonly object[] LengthCases =
        {
        new object[] {new List<string> { "tenet", "mom", "deed", "apple" }, 5, new List<string> { "tenet", "apple" } },
        new object[] {new List<string> { "tenet", "mom", "deed", "apple" }, 3, new List<string> { "mom" } },
        new object[] {new List<string> { "tenet", "apple", "1", "lol"}, 0, new List<string>() },
        };
        #endregion
    }
}