﻿
namespace List.WordList
{
    class FileDecoder : IDataDecoder
    {
        private string pathToFile;
        private char separator;

        public FileDecoder(string pathToExitFile, char separator)
        {
            pathToFile = pathToExitFile;
            this.separator = separator;
        }

        public void Decode(string text)
        {
            File.AppendAllText(pathToFile, text + separator);
        }
    }
}
