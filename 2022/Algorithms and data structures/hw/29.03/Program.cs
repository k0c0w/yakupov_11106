﻿namespace List.WordList
{
    public static class Program
    {
        static void Main()
        {
            string path = GetPath("enter path to source file:");
            Console.WriteLine("with char[] separrator:");
            char[] separator = Console.ReadLine().Split().SelectMany(symb => symb).ToArray();
            var reader = new FileReader(path, separator);
            path = GetPath("enter path to exit file");
            var coder = new FileDecoder(path, '\n');

            var list = WordList.Encode(reader);
            Run(list);
            list.Decode(coder);
        }

        static void Run(WordList list)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Possible commands:\n add word \n remove word\n stop\n \nexample: add apple");
            list.InteractiveMode(Console.ReadLine);
        }

        static string GetPath(string messegae)
        {
            Console.WriteLine(messegae);
            string result;
            while (true)
            {
                result = Console.ReadLine();
                if (string.IsNullOrEmpty(result))
                    continue;
                break;
            }
            return result;
        }
    }
}
        