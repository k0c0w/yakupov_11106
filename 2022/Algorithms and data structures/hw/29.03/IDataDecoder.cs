﻿
namespace List.WordList
{
    public interface IDataDecoder
    {
        public void Decode(string text);
    }
}
