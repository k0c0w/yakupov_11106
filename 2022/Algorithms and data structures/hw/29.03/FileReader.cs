﻿namespace List.WordList
{
    public class FileReader : IDataReader
    {
        private string pathToFile;
        private char[] separator;
        public IEnumerable<string> Read()
        {
            foreach (string line in File.ReadLines(pathToFile))
            {
                foreach (var word in line.Split(separator, StringSplitOptions.RemoveEmptyEntries))
                    yield return word;
            }
        }

        public FileReader(string path, char[] separator)
        {
            this.pathToFile = path;
            this.separator = separator;
        }
    }
}
