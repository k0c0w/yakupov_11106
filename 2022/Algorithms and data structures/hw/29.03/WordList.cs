﻿namespace List.WordList
{
    public class WordList
    {
        // this value is used to seperate by first letter 
        private string vowels = "AEIOUYАИЕЁОУЫЭЮЯ";

        private Node? Head { get;  set; }
        private Node? Tail { get;  set; }
        /// <summary>
        /// instances of class are used to store the data.
        /// Node objects are connected into a chain
        /// Head and Tail in WordList class are start and stop positions
        /// </summary>
        class Node
        {
            public string Data { get; init; }
            public Node? Next { get; set; }

            public int DataHash { get; init; }

            public Node(string word, int wordHash)
            {
                DataHash = wordHash;
                Data = word;
            }
        }
        /// <summary>
        /// starts interactive mode
        /// works until stop command.
        /// Must take predicate of command getting
        /// Available commands:[add item, remove item]
        /// </summary>
        /// <param name="getCommand">
        /// </param>
        public void InteractiveMode(Func<string> getCommand)
        {
            while (true)
            {
                var command = getCommand();
                if (string.IsNullOrEmpty(command))
                {
                    continue;
                }
                else if(command == "stop")
                {
                    break;
                }
                else
                {
                    var parsed = command.Split(' ', StringSplitOptions.RemoveEmptyEntries);
                    if(parsed.Length > 2)
                    switch (parsed[0])
                    {
                        case "add":
                            this.Add(parsed[1]);
                            break;
                        case "remove":
                            this.Remove(parsed[1]);
                            break;
                    }
                }
            }
        }
        /// <summary>
        /// Add word to WordList if it is not in yet
        /// </summary>
        /// <param name="word"></param>
        public void Add(string word)
        {
            // O(Contains + GetHash)
            int wordHash = GetHash(word);
            if(!string.IsNullOrEmpty(word) && !this.Contains(word, wordHash))
            {
                AddToChain(word, wordHash);
            }
        }
        /// <summary>
        /// Removes item if it is in.
        /// Returns true if word was removed else - false
        /// </summary>
        /// <param name="word"></param>
        /// <returns></returns>
        public bool Remove(string word)
        {
            // O(WordList.Count + GetHash)
            bool isRemoved = false;
            if(!string.IsNullOrEmpty(word))
            {
                var temHash = GetHash(word);
                var currentNode = Head;
                Node previous = null!;
                while(currentNode != null)
                {
                    if (currentNode.DataHash == temHash && currentNode.Data.Equals(word))
                    {
                        RemoveElement(currentNode, previous);
                        isRemoved = true;
                        break;
                    }
                    previous = currentNode;
                    currentNode = currentNode.Next;
                }
            }

            return isRemoved;
        }
        /// <summary>
        /// Links transmitted WordList
        /// to current instance
        /// </summary>
        /// <param name="list"></param>
        public void Link(WordList list)
        {
            // O(constant)
            if (list == this)
                return;
            if(Head == null)
            {
                Head = list.Head;
                Tail = list.Tail;
            }
            else if (list != null)
            {
                Tail!.Next = list.Head;
                Tail = list.Tail!;
            }
        }
        /// <summary>
        /// Returns new WordList with word`s length
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public WordList WithWordsLength(int length) 
        {
            // O(WordList.Count)
            var list = new WordList();
            var current = Head;
            while(current != null)
            {
                if(current.Data.Length == length)
                {
                    list.Add(current.Data);
                }
                current = current.Next;
            }
            
            return list;
        }
        /// <summary>
        /// Removes all palindromic words
        /// </summary>
        public void RemovePalindroms() 
        {
            // O(WordList.Count)
            var current = Head;
            Node previous = null!;
            while(current != null)
            {
                if (IsPalindrom(current.Data))
                {
                    RemoveElement(current, previous);
                    current = current.Next;
                    continue;
                }
                previous = current;
                current = current.Next;
            }
        }
        /// <summary>
        /// Returns pair of WordLists
        /// wich words starts with vowels and consonants
        /// </summary>
        /// <returns></returns>
        public Tuple<WordList,WordList> SplitByVowelsAndConsonants() 
        {
            var consonantWords = new WordList();
            var vowelWords = new WordList();
            var current = Head;
            while(current != null)
            {
                if (char.IsLetter(current.Data[0]))
                {
                    if (IsVowel(current.Data[0]))
                        vowelWords.Add(current.Data);
                    else
                        consonantWords.Add(current.Data);
                }
                current = current.Next;
            }
            return Tuple.Create(vowelWords, consonantWords);
        }
        /// <summary>
        /// Returns WordList with words read from fiile
        /// </summary>
        /// <returns>new WordList filled with words</returns>
        public static WordList Encode(IDataReader reader) 
        {
            var list = new WordList();
            foreach (string word in reader.Read())
            {
                list.Add(word);
            }
            return list;
        }
        /// <summary>
        /// Write all words out with separator.
        /// Deletes WordList
        /// </summary>
        public void Decode(IDataDecoder decoder) 
        {
            var current = Head;
            while(current != null)
            {
                decoder.Decode(current.Data);
                current = current.Next;
            }
            Head = null!;
            Tail = null!;
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        public void SetVowels(string vowels)
        {
            if (string.IsNullOrEmpty(vowels))
                return;
            this.vowels = vowels.ToUpper();
        }

        // this method is only needed for testing structure 
        public System.Collections.Generic.List<string> GetWords()
        {
            var currentNode = Head;
            var result = new System.Collections.Generic.List<string>();
            while(currentNode != null)
            {
                result.Add(currentNode.Data);
                currentNode = currentNode.Next;
            }
            return result;
        }
        /// <summary>
        /// Adds word into current Node chain
        /// </summary>
        /// <param name="word"></param>
        /// <param name="wordHash"></param>
        private void AddToChain(string word, int wordHash)
        {
            // O(constant)
            var element = new Node(word, wordHash);
            if (Head == null)
            {
                Head = element;
                Tail = Head;
            }
            else
            {
                Tail!.Next = element;
                Tail = element;
            }
        }

        //method checkes if word is palindrom
        private bool IsPalindrom(string word)
        {
            // O(word.Length)
            for (int i = 0; i < word.Length / 2; i++)
            {
                if (char.ToUpper(word[i]) != char.ToUpper(word[word.Length - i - 1]))
                    return false;
            }
            return true;
        }
        /// <summary>
        /// Removes element from chain
        /// </summary>
        /// <param name="element"></param>
        /// <param name="previous"></param>
        private void RemoveElement(Node element, Node previous)
        {
            // O(constant)
            if (element == Head)
            {
                if (element == Tail)
                {
                    Head = null!;
                    Tail = null!;
                }
                else
                {
                    Head = Head.Next;
                }
            }
            else if (element != null)
            {
                var next = element.Next;
                if (previous != null)
                    previous.Next = next;
            }
        }
        /// <summary>
        /// Returns word`s hash (polynomial hash)
        /// </summary>
        /// <param name="word"></param>
        /// <returns></returns>
        private static int GetHash(string word)
        {
            // o(word.Length)
            unchecked
            {
                int hash = 0;
                int prime = 9973;
                foreach (char letter in word)
                {
                    hash = hash * prime + letter;
                }
                return hash;
            }
        }
        /// <summary>
        /// Returns if word in chain
        /// </summary>
        /// <param name="word"></param>
        /// <param name="wordHash"></param>
        /// <returns></returns>
        private bool Contains(string word, int wordHash)
        {
            // O(word.Count)
            var current = Head;
            while (current != null)
            {
                if (current.DataHash == wordHash && current.Data == word)
                    return true;
                current = current.Next;
            }
            return false;
        }
        /// <summary>
        /// Returns if letter is vowel established in class
        /// </summary>
        /// <param name="letter"></param>
        /// <returns></returns>
        private bool IsVowel(char letter)
        {
            // O(vowels.Length)
            var l = char.ToUpper(letter);
            return this.vowels.Contains(l);
        }
    }
}