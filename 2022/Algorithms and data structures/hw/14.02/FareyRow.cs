﻿
namespace Cyberpunk_2088
{
    static class FareyRow
    {
        public static List<Fraction> Create(int step)
        {
            if (step < 1) throw new System.ArgumentException("Incorrect step");
            var result = new List<Fraction>();
            result.Add(new Fraction(0, 1));
            result.Add(new Fraction(1, 1));
            if (step == 1) return result;
            int currentStep = 1;
            var current = result.Head;
            while (currentStep <= step)
            {
                while (current.next != null)
                {
                    if (current.data.Denominator + current.next.data.Denominator == currentStep)
                        result.InsertAfter(current,
                            new Fraction(current.data.Nominator + current.next.data.Nominator,
                            current.data.Denominator + current.next.data.Denominator));
                    current = current.next;
                }
                current = result.Head; 
                currentStep++;
            }
            return result;
        }
    }

    class Fraction
    {
        public int Nominator { get; private set; }
        public int Denominator { get; private set; }

        public Fraction(int nominator, int denominator)
        {
            Nominator = nominator;
            Denominator = denominator;
        }

        public override string ToString()
        {
            return string.Format("{0}/{1}", Nominator, Denominator);
        }
    }

    class Node<T>
    {
        public Node<T> next;
        public Node<T> previous;
        public T data;
        public Node(T data, Node<T> next, Node<T> previous)
        {
            this.data = data;
            this.next = next;
            this.previous = previous;
        }
    }

    class List<T>
    {
        public Node<T> Head { get; private set; }
        public Node<T> Last { get; private set; }
        public int Count { get; private set; }

        public void Add(T item)
        {
            if (Head == null)
            {
                Head = new Node<T>(item, null, null);
                Last = Head;
            }
            else
            {
                var node = new Node<T>(item, null, Last);
                Last.next = node;
                Last = node;
            }
            Count++;
        }

        public void InsertAfter(Node<T> currentNode, T item)
        {
            var temp = currentNode.next;
            currentNode.next = new Node<T>(item, temp, currentNode);
            temp.previous = currentNode.next;
            Count++;
        }
    }
}
