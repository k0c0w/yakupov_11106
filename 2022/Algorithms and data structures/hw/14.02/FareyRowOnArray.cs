﻿using System;
namespace Cyberpunk_2088
{
    static class FareyRowOnArray
    {
        public static Fraction[] Create(int step)
        {
            if (step < 1) throw new System.ArgumentException("Incorrect step");
            var result = new Node[(step == 1) ? 2 : step * step] ;
            result[0] = new Node(-1,1, new Fraction(0,1));
            result[1] = new Node(0,0, new Fraction(1,1));
            int currentStep = 1;
            int insertID = 2;
            int totalCount = 2;
            while (currentStep <= step)
            {
                int currentID = 0;
                int next = result[currentID].nextID;
                while (true)
                {
                    if (next == 0) break;
                    if(result[currentID].Data.Denominator + result[next].Data.Denominator == currentStep)
                    {
                        result[insertID] = new Node(currentID, next,
                            new Fraction(result[currentID].Data.Nominator + result[next].Data.Nominator,
                            result[currentID].Data.Denominator + result[next].Data.Denominator));
                        result[result[currentID].nextID].previousID = insertID;
                        result[currentID].nextID = insertID;
                        insertID++;
                        totalCount++;
                    }
                    currentID = next;
                    next = result[currentID].nextID;
                }
                currentStep++;
            }
            return GetResultArray(totalCount, result);
        }

        private static Fraction[] GetResultArray(int len, Node[] array)
        {
            Fraction[] result = new Fraction[len];
            int CurrentID = 0;
            int i = 0;
            while (true)
            {
                if (CurrentID == 0 && i != 0) break;
                result[i++] = array[CurrentID].Data;
                CurrentID = array[CurrentID].nextID;
            }
            return result;
        }
    }

    class Node
    {
        public int previousID;
        public int nextID;
        public Fraction Data { get; private set; }
        public Node(int previous, int next, Fraction data)
        {
            previousID = previous;
            nextID = next;
            Data = data;
        }
    }
    class Fraction
    {
        public int Nominator { get; private set; }
        public int Denominator { get; private set; }

        public Fraction(int nominator, int denominator)
        {
            Nominator = nominator;
            Denominator = denominator;
        }

        public override string ToString()
        {
            return string.Format("{0}/{1}", Nominator, Denominator);
        }
    }
}
