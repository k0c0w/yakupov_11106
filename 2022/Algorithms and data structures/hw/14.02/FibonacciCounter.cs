﻿using System;

namespace Cyberpunk_2088
{
    public static class FibonacciCounter
    {
        private static string exceptionMessage = "Fibonacci position cannot be negative";
        public static long RecursiveWay(int number)
        {
            if (number <= 0) throw new ArgumentException(exceptionMessage);
            return ReccurentFib(number);
        }
        public static long CycleWay(int number)
        {
            if (number <= 0) throw new ArgumentException(exceptionMessage);
            long result = 0;
            long b = 1;
            long temporary;
            for (int i = 0; i < number; i++)
            {
                temporary = result;
                result = b;
                b += temporary;
            }
            return result;
        }

        public static long MatrixWay(int number)
        {
            if (number < 0) throw new ArgumentException(exceptionMessage);
            if (number <= 1) return number;
            long[,] matrix = new long[,] { { 1, 1 }, { 1, 0 } };
            long[,] resultMatrix = new long[,] { { 1, 0 }, { 0, 1 } };
            while(number > 0)
            {
                if( number %2 == 1)
                    MultiplyMatrix(resultMatrix, matrix);
                number /= 2;
                MultiplyMatrix(matrix, matrix);
            }
            return resultMatrix[1, 0];
        }

        private static void MultiplyMatrix(long[,] resultMatrix, long[,] matrix)
        {
            long a = resultMatrix[0, 0] * matrix[0, 0] + resultMatrix[0, 1] * matrix[1, 0];
            long b = resultMatrix[0, 0] * matrix[0, 1] + resultMatrix[0, 1] * matrix[1, 1];
            long c = resultMatrix[1, 0] * matrix[0, 0] + resultMatrix[1, 1] * matrix[0, 1];
            long d = resultMatrix[1, 0] * matrix[0, 1] + resultMatrix[1, 1] * matrix[1, 1];
            resultMatrix[0, 0] = a;
            resultMatrix[0, 1] = b;
            resultMatrix[1, 0] = c;
            resultMatrix[1, 1] = d;
        }

        private static long ReccurentFib(int number)
        {
            if (number == 0 || number == 1) return number;
            return ReccurentFib(number - 1) + ReccurentFib(number - 2);
        }
    }
}