﻿using NUnit.Framework;
using Cyberpunk_2088;

namespace TestProject3
{
    [TestFixture]
    public class Sorter_Test
    {
        [TestCase(new[] { "*", "**", "*#", "$" }, new[]{ '*', '#', '$' }, ' ', new[] { "*", "$","**", "*#"} )]
        [TestCase(new[] { "*", "$", "*"}, new[]{ '*', '#', '$' }, ' ', new[] { "*", "*","$"} )]
        [TestCase(new[] { "*"}, new[]{ '*', '#', '$' }, ' ', new[] { "*" } )]
        [TestCase(new[] { "abd", "abc", "d"}, new[]{ 'a', 'b', 'c', 'd' }, ' ', new[] { "d", "abc", "abd" } )]
        [TestCase(new[] { "abd", "abc", "d"}, new[]{ 'a', 'b', 'c', 'd' }, ' ', new[] { "d", "abc", "abd" } )]
        public void RatixTest(string[] array, char[] alphabet, char separator, string[] expect)
        {
            var sorter = new Sorter();
            var result = sorter.RadixSort(array, alphabet, separator);

            Assert.AreEqual(expect.Length, result.Length);
            for(int i=0; i<expect.Length; i++)
            {
                Assert.AreEqual(expect[i], result[i]);
            }
        }
        [Test]
        public void SeparatorInAlphabet_Test()
        {
            var separator = '1';
            var alphabet = new[] { '2', '1', '3' };
            var array = new[] { "21", "21" };
            var sorter = new Sorter();
            bool isCatshed = false;

            try
            {
                sorter.RadixSort(array, alphabet, separator);
            }

            catch (System.ArgumentException)
            {
                isCatshed = true;
            }

            Assert.AreEqual(true, isCatshed);
        }
    }
}
