﻿using System;
using System.Collections.Generic;


namespace Cyberpunk_2088
{
    public class Sorter
    {
        public string[] RadixSort(string[] arrayToSort, char[] alphabet, char separator)
        {
            if (arrayToSort == null || alphabet == null) throw new NullReferenceException();
            if (arrayToSort.Length == 1) return arrayToSort;
            Dictionary<char, Queue<string>> queueSorter = GetDictionary(alphabet,separator);
            int length;
            Queue<string> queue = GetProcessedQueue(arrayToSort, alphabet, separator, out length);
            for(int letter = length - 1; letter >= 0; letter--)
            {
                for(int i = 0; i < arrayToSort.Length; i++)
                {
                    queueSorter[queue.FirstValue[letter]].EnqueueFrom(queue);
                }
                foreach(var value in queueSorter.Values)
                {
                    if (value.Head == null) continue;
                    queue.Join(value);
                    value.Clear();
                }
            }
            return ConvertToArray(queue, separator);
        }

        private string[] ConvertToArray(Queue<string> queue, char separator)
        {
            var builder = new System.Text.StringBuilder();
            int repetitions = queue.Count;
            var result = new string[repetitions];
            for (int i = 0; i < repetitions; i++)
            {
                builder.Append(queue.Dequeue());
                int j = 0;
                while (builder[j] == separator)
                    j++;
                builder.Remove(0, j);
                result[i] = builder.ToString();
                builder.Clear();
            }

            return result;
        }

        private Dictionary<char,Queue<string>> GetDictionary(char[] alphabet, char symbol)
        {
            var result = new Dictionary<char, Queue<string>>();
            result.Add(symbol, new Queue<string>());
            foreach(var letter in alphabet)
            {
                if (letter == symbol) throw new ArgumentException("Separator is in alphabet!");
                result.Add(letter, new Queue<string>());
            }
            return result;
        }
        private Queue<string> GetProcessedQueue(string[] words, char[] alphabet, char symbol, out int maxLength)
        {
            maxLength = 0;
            var builder = new System.Text.StringBuilder();
            var queue = new Queue<string>();
            foreach(var word in words)
            {
                if(word.Length > maxLength)
                {
                    maxLength = word.Length;
                }
            }
            foreach(var word in words)
            {
                if (word.Length < maxLength) 
                {
                    //string.Concat(new string(symbol, length - word.Length), word); или +
                    builder.Append(new string(symbol,(maxLength - word.Length)));
                    builder.Append(word);
                    queue.Enqueue(builder.ToString());
                    builder.Clear();
                }
                else
                {
                    queue.Enqueue(word);
                }
            }
            return queue;
        }

        class Queue<T>
        {
            public Node<T> Tail { get; set; }
            public Node<T> Head { get; set; }
            public int Count { get; set; }
            public T FirstValue
            {
                get
                {
                    if (Head == null) throw new InvalidOperationException("Queue is empty!");
                    return Head.Data;
                }
            }

            public void Enqueue(T item)
            {
                var newItem = new Node<T>(item, null);
                if (Head == null)
                {
                    Head = newItem;
                }
                else
                {
                    Tail.Next = newItem;
                }
                Tail = newItem;
                Count++;
            }
            public void EnqueueFrom(Queue<T> queue)
            {
                if (Head == null) Head = queue.Head;
                else Tail.Next = queue.Head;
                Tail = queue.Head;
                queue.Head = queue.Head.Next;
                Tail.Next = null;
                Count++;
                queue.Count--;
                if (queue.Count == 0)
                    queue.Tail = null;
            }

            public T Dequeue()
            {
                if (Head == null) throw new InvalidOperationException();
                var current = Head;
                Head = current.Next;
                current.Next = null;
                Count--;
                return current.Data;
            }

            public void Clear()
            {
                Head = null;
                Tail = null;
                Count = 0;
            }

            public void Join(Queue<T> queue)
            {
                if (Head == null)
                    Head = queue.Head;
                else
                    Tail.Next = queue.Head;
                Tail = queue.Tail;
                Count += queue.Count;
            }
        }
        class Node<T>
        {
            public Node<T> Next { get; set; }
            public readonly T Data;

            public Node(T data, Node<T> next)
            {
                Next = next;
                Data = data;
            }
        }
    }
}
