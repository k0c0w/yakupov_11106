﻿using Cyberpunk_2088;
using NUnit.Framework;

namespace TestProject3
{
    [TestFixture]
    public class Stack_Tests
    {
        [Test]
        public void StackPushPop_Test()
        {
            var stack = new Stack<int>();
            int resultHasToBe2;
            int resultHasToBe3;

            stack.Push(0);
            stack.Push(2);
            resultHasToBe2 = stack.Pop();
            stack.Push(3);
            resultHasToBe3 = stack.Pop();

            Assert.AreEqual(resultHasToBe2, 2);
            Assert.AreEqual(resultHasToBe3, 3);
        }

        [Test]
        public void IsEmptyExeptiontest()
        {
            var stack = new Stack<int>();
            bool exeptionWasCaught = false;
            try
            {
                stack.Pop();
            }
            catch (System.InvalidOperationException)
            {
                exeptionWasCaught = true;
            }
            Assert.IsTrue(exeptionWasCaught);
        }
    }
}
