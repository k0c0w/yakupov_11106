﻿using System;

namespace Cyberpunk_2088
{
    public class Stack<T>
    {
        Node<T> last;
        class Node<T>
        {
            public Node<T> previos { get; set; }
            public readonly T data;
            public Node(T data, Node<T> previos)
            {
                this.data = data;
                this.previos = previos;
            }
        }


        public Stack()
        {
            last = null;
        }

        public void Push(T item)
        {
            last = new Node<T>(item, last);
        }

        public T Pop()
        {
            if (last == null) throw new InvalidOperationException("Stack is empty");
            T data = last.data;
            last = last.previos;
            return data;
        }
    }
}
