﻿using NUnit.Framework;
using Cyberpunk_2088;

namespace TestProject3
{
    [TestFixture]
    public class QueueOnStack_Tests
    {
        [Test]
        public void EnqueueDequeueTest()
        {
            var queue = new QueueOnStack<object>();
            var array = new string[] { "it", "is", "ref" };

            queue.Enqueue(array);
            queue.Enqueue(5);
            queue.Enqueue('5');
            var result = queue.Dequeue();
            Assert.AreEqual(array, result);
        }

        [Test]
        public void IsEmptyExeption_Test()
        {
            var queue = new QueueOnStack<int>();
            bool exeptionWasCaught = false;

            try
            {
                queue.Dequeue();
            }
            catch (System.InvalidOperationException)
            {
                exeptionWasCaught = true;
            }

            Assert.IsTrue(exeptionWasCaught);
        }
    }
}
