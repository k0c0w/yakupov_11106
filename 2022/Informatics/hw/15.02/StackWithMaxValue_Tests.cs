using NUnit.Framework;
using System;
using Cyberpunk_2088;

namespace TestProject3
{

    [TestFixture]
    public class StackWithMaxValue_Tests
    {
        [TestCase(5)]
        [TestCase("avs")]
        public void StackPushPop_Test<T>(T entry) where T : IComparable
        {
            var stack = new StackWithMaxValue<T>();
            stack.Push(entry);
            var result = stack.Pop();

            Assert.AreEqual(entry, result);
        }

        [TestCase(new[] { 2, 6, 5 })]
        [TestCase(new[] { 2, 6, 25 })]
        [TestCase(new[] { 20, 6, 5 })]
        [TestCase(new[] { 6, 6, 5 })]
        [TestCase(new[] { 6, 0.0, 1, 6.5, 6 })]
        [TestCase(new[] { 'b', 'a', 'z', 'y', 'c' })]
        public void MaxValue_Test<T>(T[] items) where T : IComparable
        {
            var stack = new StackWithMaxValue<T>();
            var max = items[0];
            for (int i = 0; i < items.Length; i++)
            {
                stack.Push(items[i]);
                if (max.CompareTo(items[i]) < 0) max = items[i];
            }

            var result = stack.GetMaxValue();

            Assert.AreEqual(max, result);
        }

        [TestCase(new[] { 1, 2, 4, 0 , 6})]
        [TestCase(new[] { 1, 2, 4, 6 , 0})]
        public void MaxValueAfterPop<T>(T[] items) where T : IComparable
        {
            var stack = new StackWithMaxValue<T>();
            var max = items[0];
            for (int i = 0; i < items.Length; i++)
            {
                if (i< items.Length - 1 && max.CompareTo(items[i]) < 0) max = items[i];
                stack.Push(items[i]);
            }
            stack.Pop();

            var result = stack.GetMaxValue();

            Assert.AreEqual(max, result);
        }

        [Test]
        public void IsEmptyExeption_Test()
        {
            var stack = new StackWithMaxValue<int>();
            bool exeptionWasCaught = false;
            bool maxValueExeptionWasCaught = false;

            try
            {
                stack.Pop();
            }
            catch (System.InvalidOperationException)
            {
                exeptionWasCaught = true;
            }

            try
            {
                stack.GetMaxValue();
            }
            catch (System.InvalidOperationException)
            {
                maxValueExeptionWasCaught = true;
            }

            Assert.IsTrue(exeptionWasCaught);
            Assert.IsTrue(maxValueExeptionWasCaught);
        }
    }
}
