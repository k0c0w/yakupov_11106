﻿using System;

namespace Cyberpunk_2088
{
    public class StackWithMaxValue<T> where T : IComparable

    {
        Node<T> last;
        protected class Node<T>
        {
            public Node<T> previos { get; set; }
            public Node<T> maxValueItem { get; set; }
            public readonly T data;
            public Node(T data, Node<T> previos)
            {
                this.data = data;
                this.previos = previos;
            }
        }

        public StackWithMaxValue()
        {
            last = null;
        }

        public void Push(T item)
        {
            var currentObject = new Node<T>(item, last);
            if (last == null || item.CompareTo(last.maxValueItem.data) >= 0) currentObject.maxValueItem = currentObject;
            else currentObject.maxValueItem = last.maxValueItem;
            last = currentObject;
        }

        public T Pop()
        {
            if (last == null) throw new InvalidOperationException("Stack is empty");
            T data = last.data;
            last = last.previos;
            return data;
        }

        public T GetMaxValue()
        {
            if (last == null) throw new InvalidOperationException("Stack is empty");
            return last.maxValueItem.data;
        }
    }
}
