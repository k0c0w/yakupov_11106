﻿using System;

namespace Cyberpunk_2088
{
    public class QueueOnStack<T>
    {
        Stack<T> mainStack;
        Stack<T> folderStack;
        private int count;


        public QueueOnStack()
        {
            mainStack = new Stack<T>();
            folderStack = new Stack<T>();
        }

        public void Enqueue(T item)
        {
            mainStack.Push(item);
            ++count;
        }

        public T Dequeue()
        {
            if (count == 0) throw new InvalidOperationException("Queue is empty!");
            int steps = --count;
            while(steps != 0)
            {
                folderStack.Push(mainStack.Pop());
                --steps;
            }
            T dequeue = mainStack.Pop();
            while(steps != count)
            {
                mainStack.Push(folderStack.Pop());
                ++steps;
            }

            return dequeue;
        }
    }
}
