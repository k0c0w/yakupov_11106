using NUnit.Framework;
using Cyberpunk_2088;
namespace TestProject1
{
    public class Tests
    {
        [Test]
        public void PushPop_Test()
        {
            var stack = new VersionalStack<int>();

            Push123(stack);
            var res1 = stack.Pop();
            var res2 = stack.Pop();

            Assert.AreEqual(3, res1);
            Assert.AreEqual(2, res2);
        }
        [Test]
        public void ForgetTest()
        {
            var stack = new VersionalStack<int>();

            Push123(stack);
            stack.Forget();

            bool catched = false;
            try
            {
                stack.RollBack(0);
            }
            catch (System.InvalidOperationException)
            {
                catched = true;
            }

            stack.Pop();
            var result = stack.Pop();

            Assert.IsTrue(catched);
            Assert.AreEqual(2, result);
        }

        [Test] 
        public void RollBack_Test1()
        {
            var stack = new VersionalStack<int>();

            Push123(stack);
            stack.RollBack(1);
            stack.Push(4);
            stack.Pop();
            var result = stack.Pop();

            Assert.AreEqual(2, result);
        }
        [Test]
        public void RollBack_Test2()
        {
            var stack = new VersionalStack<int>();

            Push123(stack);
            stack.Pop();
            stack.Pop();
            stack.Pop();
            stack.Push(4);
            stack.RollBack(0);
            var result = stack.Pop();

            Assert.AreEqual(1, result);
        }
        [Test]
        public void RollBack_Test3()
        {
            var stack = new VersionalStack<int>();

            Push123(stack);
            stack.RollBack(1);
            stack.Push(4);
            stack.RollBack(2);
            var result = stack.Pop();

            Assert.AreEqual(3, result);
        }

        private void Push123(VersionalStack<int> stack)
        {
            stack.Push(1);
            stack.Push(2);
            stack.Push(3);
        }
    }
}