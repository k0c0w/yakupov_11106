﻿using System;
using System.Collections.Generic;

namespace Cyberpunk_2088
{
    public class VersionalStack<T>
    {
        List<Node<T>> headHistory;
        Stack<T> stack;
        Node<T> emptyItem;
        public int avalibleRollbacks { get { return headHistory.Count; } }

        public VersionalStack()
        {
            headHistory = new List<Node<T>>();
            stack = CreateStack();
        }

        public void Push(T item)
        {
            stack.Push(item);
            headHistory.Add(stack.Head);
        }

        public T Pop()
        {
            if (stack.Head == emptyItem) throw new InvalidOperationException();
            var result = stack.Pop();
            headHistory.Add(stack.Head);
            return result;
        }

        public void RollBack(int headPosition)
        {
            if(headPosition >= headHistory.Count || headPosition < 0 || headHistory[headPosition] == null) throw new InvalidOperationException();
            stack.Head = headHistory[headPosition];
            headHistory.Add(stack.Head);
        }

        public void Cancel()
        {

        }

        public void Forget()
        {
            headHistory = new List<Node<T>>();
        }
        private Stack<T> CreateStack()
        {
            var stack = new Stack<T>();
            stack.Push(default);
            emptyItem = stack.Head;
            return stack;
        }
    }

    class Stack<T>
    {
        public Node<T> Head { get; set; }

        public void Push(T item)
        {
            var current = new Node<T>(item);
            if(Head == null)
            {
                Head = current;
            }
            else
            {
                current.Previous = Head;
                Head = current;
            }
        }

        public T Pop()
        {
            if (Head == null) throw new InvalidOperationException("Stack is empty!");
            var current = Head;
            Head = current.Previous;
            return current.data;
        }

        public void Cancel() { }
    }

    class Node<T>
    {
        public readonly T data;
        public Node<T> Previous { get; set; }
        public Node(T data)
        {
            this.data = data;
        }
    }
}
