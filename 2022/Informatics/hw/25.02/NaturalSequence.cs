﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Cyberpunk_2088
{
    
    public class NaturalSequence : IEnumerable
   {
        public IEnumerator GetEnumerator()
        {
            return new NaturalEnumenator();
        }
    }

    public class NaturalEnumenator : IEnumerator
    {
        private int current;
        public int Current
        {
            get { return current; }
        }
        object IEnumerator.Current => Current;
        public NaturalEnumenator()
        {
            current = 0;
        }
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public bool MoveNext()
        {
            ++current;
            return current > 0;
        }

        public void Reset()
        {
            current = 0;
        }
    }
}