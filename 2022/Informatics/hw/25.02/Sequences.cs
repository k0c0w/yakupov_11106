﻿using System.Collections.Generic;
namespace Cyberpunk_2088
{
   public static class Sequences
   {
        public static IEnumerable<int> NaturalSequence()
        {
            int a = 1;
            while (true)
            {
                yield return a++;
                if (a < 0)
                    break;
            }
        }
   }
}