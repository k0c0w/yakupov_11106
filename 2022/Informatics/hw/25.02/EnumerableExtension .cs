﻿using System.Collections.Generic;
namespace Cyberpunk_2088
{
   public static class EnumerableExtension 
   {
        public static IEnumerable<T> Take<T>(this IEnumerable<T> collection, int n)
        {
            int count = 0;
            foreach(var elem in collection)
            {
                if (count++ == n) yield break;
                yield return elem;
            }
        }
   }
}