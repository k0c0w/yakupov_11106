using NUnit.Framework;
using System.Collections.Generic;
using Cyberpunk_2088;
using System.Collections;

namespace TestProject1
{
    public static class ToTest
    {
        public static IEnumerable DoSomething(IEnumerable obj)
        {
            var result = new List<object>();
            foreach(var item in obj)
                foreach(var item2 in obj)
                    result.Add(item2);
            return result;
        }
    }

    class SpecialCollection<T> : IEnumerable<T>
    {
        public int Count { get; private set; }
        T[] array;
        public SpecialCollection(int n)
        {
            array = new T[n];
            Count = n;
        }
        public IEnumerator<T> GetEnumerator()
        {
            return GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return new SpecialEnumerator<T>(this);
        }
    }

    class SpecialEnumerator<T> : IEnumerator<T>
    {
        private SpecialCollection<T> current;
        private int currentItem = 0;
        public T Current { get; private set; }
        public static int enumsCount = 0;
        public SpecialEnumerator(SpecialCollection<T> collection)
        {
            enumsCount++;
            current = collection;
        }
        object IEnumerator.Current { get { return Current; } }

        public void Dispose()
        {
        }
        public bool MoveNext()
        {
            if (currentItem == current.Count) return false;
            else
            {
                currentItem++;
            }
            return true;
        }
        public void Reset()
        {
        }
    }

    class EmptyCollection<T> : SpecialCollection<T>, IEnumerable<T>
    {
        public EmptyCollection():base(0)
        { 
        }
        public IEnumerator<T> GetEnumerator()
        {
            return GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return new SpecialEnumerator<T>(this);
        }
    }

    [TestFixture]
    public class Tests
    {
        [Test]
        public static void DoublePass()
        {
            var testCollection = new SpecialCollection<int>(5);

            ToTest.DoSomething(testCollection);

            var result = SpecialEnumerator<int>.enumsCount > 1;

            Assert.AreEqual(false, result, "Method gets 2 enumerators");
        }
        [Test]
        public static void LazyTest()
        {
            var collection = new EmptyCollection<int>();
            ToTest.DoSomething(collection);

            bool result = SpecialEnumerator<int>.enumsCount > 0;

            Assert.AreEqual(result, false, "Is not too lazy");
        }
    }
}