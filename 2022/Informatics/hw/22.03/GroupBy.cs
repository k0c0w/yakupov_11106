﻿namespace kek
{
    public static class LinqExtension
    {
        public static IEnumerable<T> GroupBy<T>(this IEnumerable<T> source, Func<T, bool> rule)
        {
            foreach (var item in source)
                if (rule(item))
                    yield return item;
        }
    }
}