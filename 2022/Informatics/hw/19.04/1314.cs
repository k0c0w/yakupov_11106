using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections;

namespace Cyberpunk_2088
{
    public class Node
    {
        public int ID { get; }

        public Node(int item)
        {
            ID = item;
        }

        public override bool Equals(object? obj)
        {
            if (obj is Node && obj != null)
            {
                var node = (Node)obj;
                return ID == node.ID;
            }

            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return ID.GetHashCode();
        }

        public override string ToString()
        {
            return ID.ToString();
        }
    }

    public class Graph
    {
        public Dictionary<Node, List<Node>> incident = new Dictionary<Node, List<Node>>();

        public void Add(Node i1, Node i2)
        {
            if (i2 == null)
            {
                if (i1 != null && !incident.ContainsKey(i1))
                    incident.Add(i1, new List<Node>());
                return;
            }
            else
            {
                AddToDictionary(i1, i2);
                AddToDictionary(i2, i1);
            }
        }

        private void AddToDictionary(Node i1, Node i2)
        {
            if (!incident.ContainsKey(i1))
                incident.Add(i1, new List<Node>());
            if (!i1.Equals(i2) && !incident[i1].Contains(i2))
                incident[i1].Add(i2);
        }

        public List<Node> this[Node index]
        {
            get { return incident[index]; }
        }
    }

   /*
4
3 75 85 20
2 61 62
5 75 20 85 50 61
3 10 20 30
3 30 20 85*/

    public class Program
    {
        static void Main()
        {
            var g = new Graph();
            var n = int.Parse(Console.ReadLine());
            for (int i = 0; i < n; i++)
            {
                var l = Parse(Console.ReadLine());
                Node left = null;
                Node right = null;
                for (var j = 0; j < l.Length; j++)
                {
                    if (j > 0)
                    {
                        left = new Node(l[j - 1]);
                    }

                    if (j < l.Length - 1)
                    {
                        right = new Node(l[j + 1]);
                    }
                    var current = new Node(l[j]);
                    g.Add(current, left);
                    g.Add(current, right);
                }
            }

            var way = Parse(Console.ReadLine()).Select(x => new Node(x)).ToArray();
            var length = way.Length;

            var waysFromLast = Bfs(way[0], g);
            var waysFromFirst = Bfs3(way[way.Length - 1], g, waysFromLast, length);

            var result = new List<Node>();
            foreach (var fromFirst in waysFromFirst.Keys)
            {
                foreach (var fromLast in waysFromLast.Keys)
                {
                    if (waysFromLast[fromLast] - waysFromFirst[fromFirst] + 1 == length
                        //&& !way.Contains(fromFirst)
                        && fromLast.Equals(fromFirst))
                        result.Add(fromLast);
                }
            }


            result.Add(way[way.Length - 1]);

            foreach (var k in result.Distinct().OrderBy(x => x.ID))
            {
                Console.WriteLine(k.ID);
            }
        }

        public static Dictionary<Node, int> Bfs3(Node startNode, Graph g, Dictionary<Node, int> way, int length)
        {

            var folder = new Dictionary<Node, int>();
            var visited = new HashSet<Node>();
            var queue = new Queue<(Node, int)>();
            queue.Enqueue((startNode, 1));
            visited.Add(queue.Peek().Item1);
            while (queue.Count != 0)
            {
                var tuple = queue.Dequeue();
                var current = tuple.Item1;
                var wayLength = tuple.Item2;
                foreach (var neighboor in g.incident[current])
                {
                    if (!visited.Contains(neighboor))
                    {
                        if (way.ContainsKey(neighboor) && way[current] < wayLength + length - 1)
                            continue;
                        queue.Enqueue((neighboor, wayLength + 1));
                    }
                }
                visited.Add(current);
                if(!folder.ContainsKey(current))
                    folder.Add(current, wayLength);
            }
            return folder;
        }
        public static Dictionary<Node, int> Bfs(Node startNode, Graph g)
        {
            var folder = new Dictionary<Node, int>();
            var visited = new HashSet<Node>();
            var queue = new Queue<(Node, int)>();
            queue.Enqueue((startNode, 1));
            visited.Add(queue.Peek().Item1);
            while (queue.Count != 0)
            {
                var tuple = queue.Dequeue();
                var current = tuple.Item1;
                var wayLength = tuple.Item2;
                foreach (var neighboor in g.incident[current])
                {
                    if (!visited.Contains(neighboor))
                    {
                        queue.Enqueue((neighboor, wayLength + 1));
                    }
                }
                visited.Add(current);
                if(!folder.ContainsKey(current))
                    folder.Add(current, wayLength);
            }
            return folder;
        }

        public static int[] Parse(string input)
        {
            return input
                .Split()
                .Skip(1)
                .Select(x => int.Parse(x))
                .ToArray();
        }
    }
}