﻿using System;
using System.Text.Json;
using System.Threading;
using System.Globalization;
using System.Linq;
using System.Net.Http.Headers;

namespace ControlWork
{
    class Parser
    {
        static async Task Main()
        {
            Console.WriteLine("Enter folder path");
            var path = Console.ReadLine();

            var url = "https://jsonplaceholder.typicode.com/photos";
            using var client = new HttpClient();

            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Add("User-agent", "C# image downloader");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var response = await client.GetAsync(url);
            try
            {
                response.EnsureSuccessStatusCode();
            }
            catch (HttpRequestException)
            {
                Console.WriteLine("Error occured");
                return;
            }

            var targetsJSONInfo = await response.Content.ReadAsStringAsync();

            var tasks = ParseAndStartDownload(targetsJSONInfo);
            Task.WaitAll(tasks);
            var count = 0;
            foreach(var image in tasks)
            {
                count++;
                var localPath = string.Format($"{path}\\image{count}.icon");
                File.WriteAllBytes(localPath, image.Result);
            }

            Console.WriteLine("download completed");
        }

        static  Task<byte[]>[] ParseAndStartDownload(string targetsJSONInfo)
        {
            using JsonDocument doc = JsonDocument.Parse(targetsJSONInfo);
            JsonElement root = doc.RootElement;

            return root.EnumerateArray()
                .AsParallel()
                .Select(imageInfo => imageInfo.GetProperty("thumbnailUrl").ToString())
                .Select(url => { var task = StartDownloading(url); return task; })
                .ToArray();
        }

        static async Task<byte[]> StartDownloading(string url)
        {
            using var client = new HttpClient();
            Thread.Sleep(100);
            return await client.GetByteArrayAsync(url);
        }
    }
}   