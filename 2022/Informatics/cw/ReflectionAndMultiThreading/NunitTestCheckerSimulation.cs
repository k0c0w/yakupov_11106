using System;
using System.Linq;
using System.Reflection;
using NUnit.Framework;

namespace ControlWork
{
    [TestFixture]
    public class WordList_Test
    {
        [SetUp]
        public static void Set()
        {
            Console.WriteLine("set up");
        }

        [Test]
        static void Testing()
        {
            Console.WriteLine("Testing");
        }

        [Test]
        static void Testing2()
        {
            Console.WriteLine("Testing 2");
        }
        static void DoSomething() { }
        void DoSomethingElse() { }
    }

    [TestFixture]
    public class WordList
    {

        [Test]
        public static void Testing()
        {
            Console.WriteLine("Testing again");
        }

        [TearDown]
        static void Tearing()
        {
            Console.WriteLine("Tearing");
        }

    }

    static class LinqAttributeExtension
    {
        public static IEnumerable<MemberInfo>? GetMethodsMarkedWithAttributeType(this IEnumerable<MemberInfo>? methods, Type attributeType)
        {
            return methods?.Where(method => method
                        .GetCustomAttributes()
                        .Any(attribute => attribute.GetType() == attributeType));
        }
    }

    class Prog
    {
        static void Main()
        {
            var testFixtureType = typeof(TestFixtureAttribute);
            var testType = typeof(TestAttribute);
            var setupType = typeof(SetUpAttribute);
            var teardownType = typeof(TearDownAttribute);

            var types = Assembly.GetExecutingAssembly()
                .GetTypes()
                .Where(type => type
                    .GetCustomAttributes()
                    .Any(attribute => attribute.GetType() == testFixtureType));
            foreach (Type type in types)
            {
                var methods = type.GetMethods(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance);
                var tests = methods?.GetMethodsMarkedWithAttributeType(testType);
                var setup = methods?.GetMethodsMarkedWithAttributeType(setupType)?.FirstOrDefault();
                var teardown = methods?.GetMethodsMarkedWithAttributeType(teardownType)?.FirstOrDefault();

                StartTesting(type, tests, setup, teardown);
            }
        }

       static void StartTesting(Type type, IEnumerable<MemberInfo>? tests, MemberInfo? setup = null, MemberInfo? teardown = null)
       {
            MethodInfo? setupMethod = null;
            MethodInfo? teardownMethod =null;
            if (setup != null)
                setupMethod = GetMethodWithoutParams(type, setup.Name);
            if(teardown != null)
                teardownMethod = GetMethodWithoutParams(type, type.Name);

            foreach(MemberInfo test in tests)
            {
                setupMethod?.Invoke(type, new object[] {});
                GetMethodWithoutParams(type, test.Name)?.Invoke(type, new object[] {});
                teardownMethod?.Invoke(type, new object[] {});
            }
       }

        static MethodInfo? GetMethodWithoutParams(Type type, string methodName)
        {
            return type.GetMethod(methodName,
                    BindingFlags.Public | BindingFlags.NonPublic
                    | BindingFlags.Instance | BindingFlags.Static);
        }

    }
}