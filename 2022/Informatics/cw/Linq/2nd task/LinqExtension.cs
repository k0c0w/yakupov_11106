﻿namespace ControlWorkLinq
{
    /// <summary>
    /// Task 2
    /// </summary>
    public static class LinqExtension
    {
        public static IEnumerable<Tuple<TItem , TItem>>
            Partition<TItem>(this IEnumerable<TItem> source1, IEnumerable<TItem> source2, Predicate<TItem> predicate)
        {
            foreach(var item1 in source1)
            {
                foreach(var item2 in source2)
                {
                    if (predicate(item1) == predicate(item2))
                        yield return Tuple.Create(item1, item2);
                }
            }
        }
    }
}
