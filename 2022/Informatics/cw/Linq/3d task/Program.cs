﻿using ControlWorkLinq;
var a = new[]
{
    new Consumer(1, 2003, "Volkov"),
    new Consumer(5, 2003, "Raispolkom"),
    new Consumer(2, 1988, "Somali"),
    new Consumer(4, 1965, "Volkov"),
    new Consumer(3, 2004, "Rashin")
};

var sodaCode = new VenderCode("AA12-3487");
var ballCode = new VenderCode("BC45-0000");
var brushCode = new VenderCode("DA56-1276");
var breadCode = new VenderCode("RR77-7778");

var pytaorka = new Shop("Пятерочка");
var maga = new Shop("Мага");
var bibi = new Shop("Би-Би");

var b = new[]
{
    new Product(sodaCode, "food", "Ukrain"),
    new Product(breadCode, "food", "USA"),
    new Product(ballCode, "toy", "China"),
    new Product(brushCode, "household items", "Usa")
};

var c = new[]
{
    new Discount(2, pytaorka, 5),
    new Discount(3, pytaorka, 14),
    new Discount(1, bibi, 1),
    new Discount(2, bibi, 49),
    new Discount(5, bibi, 35),
};

var d = new[]
{
    new Price(100, bibi, brushCode),
    new Price(150, pytaorka, brushCode),
    new Price(50, pytaorka, ballCode),
    new Price(20, pytaorka, breadCode),
    new Price(25, maga, breadCode),
    new Price(89, maga, sodaCode),
    new Price(49, maga, ballCode)
};

var e = new[]
{
    new Purchase(1, pytaorka, breadCode),
    new Purchase(2, pytaorka, breadCode),
    new Purchase(1, pytaorka, breadCode),
    new Purchase(2, bibi, brushCode),
    new Purchase(2, maga, ballCode),
    new Purchase(3, maga, sodaCode),
    new Purchase(5, maga, breadCode),
    new Purchase(5, bibi, brushCode)
};

var result = e.GroupBy(purchase => purchase.ConsumerCode)
    .Select(group => Tuple.Create(group.Key, group.Select(purchase => purchase.VenderCode).Distinct().Count()));
foreach (var item in result)
    Console.WriteLine(item);