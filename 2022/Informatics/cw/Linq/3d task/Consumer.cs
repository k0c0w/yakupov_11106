﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlWorkLinq
{
    public class Consumer
    {
        public int ConsumerCode { get; init; }

        public int BirthDate { get; init; }

        public string Street { get; init;}

        public Consumer(int consumerCode, int year, string street)
        {
            ConsumerCode = consumerCode;
            BirthDate = year;
            Street = street;
        }
    }
}
