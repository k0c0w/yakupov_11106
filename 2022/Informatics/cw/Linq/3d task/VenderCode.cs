﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlWorkLinq
{
    internal class VenderCode
    {
        public string ProductCode { get; init; }

        public VenderCode(string code)
        {
            ProductCode = code;
        }

        public override bool Equals(object? obj)
        {
            if (obj != null && obj is VenderCode)
            {
                var code = (VenderCode)obj;
                return ProductCode == code.ProductCode;
            }
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return ProductCode.GetHashCode();
        }

        public override string ToString()
        {
            return ProductCode.ToString();
        }
    }
}
