﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlWorkLinq
{
    internal class Product
    {
        public VenderCode VenderCode { get; private set; }

        public string Category { get; private set; }

        public string OriginCountry { get; private set; }

        public Product(VenderCode code, string category, string origin)
        {
            VenderCode = code;
            Category = category;
            OriginCountry = origin;  
        }
    }
}
