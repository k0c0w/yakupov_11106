﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlWorkLinq
{
    internal class Discount
    {
        public int PercentValue { get; private set; }

        public Shop ShopName { get; private set; }

        public int ConsumerCode { get; private set; }

        public Discount(int code, Shop shop, int discount)
        {
            ConsumerCode = code;
            ShopName = shop;
            PercentValue = discount;
        }
    }
}
