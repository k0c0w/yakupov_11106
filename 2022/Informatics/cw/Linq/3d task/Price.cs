﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlWorkLinq
{
    internal class Price
    {
        public int PriceInRubles { get; private set; }

        public Shop ShopName { get; private set; }

        public VenderCode VenderCode { get; private set; }

        public Price(int price, Shop shop, VenderCode code)
        {
            PriceInRubles = price;
            ShopName = shop;
            VenderCode = code;
        }
    }
}
