﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlWorkLinq
{
    internal class Shop
    {
        public string Name { get; init; }

        public Shop(string name)
        {
            Name = name;
        }

        public override bool Equals(object? obj)
        {
            if (obj != null && obj is Shop)
            {
                var shop = (Shop)obj;
                return Name == shop.Name;
            }
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }

        public override string ToString()
        {
            return Name.ToString();
        }
    }
}
