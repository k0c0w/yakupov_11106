﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlWorkLinq
{
    internal class Purchase
    {
        public int ConsumerCode { get; init; }

        public Shop Shop { get; init; }

        public VenderCode VenderCode { get; init; }

        public Purchase(int consumerCode, Shop shop, VenderCode venderCode)
        {
            ConsumerCode = consumerCode;
            Shop = shop;
            VenderCode = venderCode;
        }
    }
}
