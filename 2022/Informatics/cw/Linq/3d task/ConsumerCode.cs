﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlWorkLinq
{
    public class ConsumerCode
    {
        public int Code { get; init; }

        public ConsumerCode(int code)
        {
            Code = code;
        }

        public override bool Equals(object? obj)
        {
            if(obj != null && obj is ConsumerCode)
            {
                var code = (ConsumerCode)obj;
                return Code == code.Code;
            }
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return Code.GetHashCode();
        }

        public override string ToString()
        {
            return Code.ToString();
        }
    }
}
