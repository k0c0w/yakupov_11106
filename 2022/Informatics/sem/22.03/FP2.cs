﻿
namespace Cyberpunk_2088
{
    public static class LinqExtension
    {
        public static IEnumerable<T> MyGroupBy<T>(this IEnumerable<T> source, Func<T, bool> rule)
        {
            foreach (var item in source)
                if (rule(item))
                    yield return item;
        }
    }

    public class DataAnalazier
    {
        public Dictionary<DayOfWeek, int> GetEveryDayLoadStatistics(string sourcePath)
        {
            var data = ReadData(sourcePath);
            return SetData(data);
        }

        private static Tuple<DateOnly, int>[] ReadData(string path) 
        {
            if(path == null) throw new ArgumentNullException("path");

            var folder = new List<Tuple<DateOnly, int>>();
                foreach(string line in File.ReadLines(path))
                { 
                    var data = line.Split(" ", StringSplitOptions.RemoveEmptyEntries);
                    int count;
                    DateOnly date;
                    try
                    {
                        int.TryParse(data[1], out count);
                        DateOnly.TryParse(data[0], out date);
                        folder.Add(Tuple.Create(date, count));
                    }
                    catch (FormatException)
                    {
                        continue;
                    }
                }
            return folder.ToArray();
        }

        private static Dictionary<DayOfWeek, int> SetData(Tuple<DateOnly, int>[] source) 
        {
            var result = new Dictionary<DayOfWeek, int>();
            for (var dayOfWeek = DayOfWeek.Sunday ; dayOfWeek <= DayOfWeek.Saturday; dayOfWeek++)
            { 
                int average = AverageCount(source,  dayOfWeek);
                result.Add(dayOfWeek, average);
            }

            return result;
        }

        private static int AverageCount(Tuple<DateOnly, int>[] source, DayOfWeek dayOfWeek)
        {
            int count = 0;
            int visits = 0;
            var folder = source
                .MyGroupBy(day => dayOfWeek == day.Item1.DayOfWeek);
            foreach (var day in folder)
            {
                visits += day.Item2;
                count++;
            }

            return (count == 0) ?  0 : visits / count;
        }
    }
}
