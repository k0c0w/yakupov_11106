namespace Cyberpunk_2088
{
    static class LinqExtenssion
    { 
        public static IEnumerable<Tuple<TItem1, TItem2>> JoinToTuple<TItem1,TItem2>(this IEnumerable<TItem1> current, 
            IEnumerable<TItem2> collection,Func<TItem1, TItem2, bool> predicate, Func<TItem1, TItem1> selector1, Func<TItem2, TItem2> selector2)
        {
            foreach(var item in current)
            {
                foreach(var secondItem in collection)
                {
                    if(predicate(item, secondItem))
                        yield return Tuple.Create(selector1(item), selector2(secondItem));
                }
            }
        }
    }
}
