﻿namespace Cyberpunk_2088
{
    record class Student
    {
        public string Name { get; init; }
        public string PathToDllFolder { get; init; }
        public IEnumerable<string> Exercises { get; init; }

    }
}
