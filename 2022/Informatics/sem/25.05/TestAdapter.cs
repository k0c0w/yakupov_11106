﻿using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.IO;

namespace Cyberpunk_2088
{
    class TestAdapter
    {
        readonly IOutPut _OutPut;

        public TestAdapter(IOutPut outPut)
        {
            _OutPut = outPut;
        }

        public  void ExecuteTests(string directory, int threadsAmount = 1)
        {
            var students = GetStudentsFromVariants(directory);
            var exercisesData = GetExercisesDataFromExercises(directory);
            var tests = GetTestDataFromTests(directory, exercisesData);

            var results = GetTestResultsForStudents(threadsAmount, students, exercisesData, tests);

            LogResultsAndReport(directory, threadsAmount, results);
        }
        
        void LogResultsAndReport(string directory, int threadsAmount, List<TestResults> results)
        {
            var portion = (int)(Math.Ceiling(results.Count / (double)threadsAmount));
            var testingProccesses = new List<Task>();
            for (var i = 0; i < results.Count; i += portion)
            {
                var studentsPortion = new List<TestResults>(portion);
                for (var j = 0; j < portion && j + i < results.Count; j++)
                    studentsPortion.Add(results[j + i]);

                var task = Task
                    .Factory.StartNew(() => LogToFile(directory, studentsPortion));
                testingProccesses.Add(task);
            }

            Report(results);

            Task.WaitAll(testingProccesses.ToArray());
        }

        void Report(List<TestResults> results)
        {
            foreach(var result in results)
            {
                _OutPut.PrintWithYellow(result.StudentName);
                foreach(var test in result.Tests)
                {
                    _OutPut.PrintF($"\t{test.TestName}\t");
                    _OutPut.PrintFWithGreen($"\tSuccess: {test.Success}\t");
                    _OutPut.PrintFWithRed($"Fail: {test.Failure}\n\n");
                }
            }
        }

        void LogToFile(string directory, List<TestResults> studentsResults)
        {
            foreach(var result in studentsResults)
            {
                if(result.Failures.Count > 0)
                {
                    try
                    {
                        var sw = new StreamWriter($"{directory}\\Results\\{result.StudentName}.txt");
                        foreach(var fail in result.Failures)
                        {
                            sw.WriteLine(fail.TestName);
                            sw.WriteLine(fail.Type);
                            if(fail.Exception != null)
                                sw.WriteLine($"Exeption: {fail.Exception}");
                            if (fail.Parameters != null)
                                sw.WriteLine($"Parameters: {fail.Parameters}");
                            sw.WriteLine();
                        }
                        sw.Close();
                    }
                    catch (Exception e)
                    {
                        _OutPut.PrintWithRed("Exception: " + e.Message);
                    }
                }
            }
        }

        private List<TestResults> GetTestResultsForStudents(int threadsAmount, List<Student> students,
            ReadOnlyDictionary<string, List<string>> exercisesData, ReadOnlyDictionary<string, List<List<string>>> tests)
        {
            var portion = (int)(Math.Ceiling(students.Count / (double)threadsAmount));
            var testingProccessIndex = 0;
            var testingProccesses = new List<Task<List<TestResults>>>();
            for (var i = 0; i < students.Count; i += portion)
            {
                var studentsPortion = new List<Student>(portion);
                for (var j = 0; j < portion && j + i < students.Count; j++)
                    studentsPortion.Add(students[j + i]);

                var task = Task<List<TestResults>>
                    .Factory.StartNew(() => TestStudents(studentsPortion, exercisesData, tests));
                testingProccesses.Add(task);
            }

            Task.WaitAll(testingProccesses.ToArray());

            return testingProccesses.SelectMany(x => x.Result).ToList();
        }

        private List<TestResults> TestStudents(List<Student> students, 
            ReadOnlyDictionary<string, List<string>> exercisesData, 
            ReadOnlyDictionary<string, List<List<string>>> tests)
        {
            var result = new List<TestResults>();

            foreach (var student in students)
            {
                var testRunner = new StudentTestsRunner(student, exercisesData, tests, _OutPut);
                result.Add(testRunner.RunTests());
            }
            
            return result;
        }

        private ReadOnlyDictionary<string, List<string>> GetExercisesDataFromExercises(string directory)
        {
            var dict = new Dictionary<string, List<string>>();
            var newPath = directory + "\\Exercises";
            foreach(var file in Directory.GetFiles(newPath))
            {
                var description = new List<string>();
                var name = file.Split('\\').Last().Split('.').FirstOrDefault();
                string line;
                try
                {
                    StreamReader sr = new StreamReader(file);
                    line = sr.ReadLine();
                    while (line != null)
                    {
                        description.Add(line);
                        line = sr.ReadLine();
                    }
                    sr.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Exception on reading file {name}: " + e.Message);
                }
                dict.Add(name, description);
            }

            return new ReadOnlyDictionary<string, List<string>>(dict);
        }

        private ReadOnlyDictionary<string, List<List<string>>> GetTestDataFromTests(string directory, 
            ReadOnlyDictionary<string, List<string>> exercises)
        {
            var dict = new Dictionary<string, List<List<string>>>();
            foreach(var key in exercises.Keys)
                dict.Add(key, GetFromCases(directory + $"\\Tests\\{key}"));

            return new ReadOnlyDictionary<string, List<List<string>>>(dict);
        }

        private List<List<string>> GetFromCases(string directory)
        {
            var tests = new List<List<string>>();
            foreach(var path in Directory.GetFiles(directory))
            {
                var testCase = new List<string>();
                string line;
                try
                {
                    StreamReader sr = new StreamReader(path);
                    line = sr.ReadLine();
                    while (line != null)
                    {
                        testCase.Add(line);
                        line = sr.ReadLine();
                    }
                    sr.Close();
                }
                catch (Exception e)
                {
                    _OutPut.PrintFWithRed(($"Exception on reading file {path}: " + e.Message));
                }
                tests.Add(testCase);
            }
            
            return tests;
        }

        private List<Student> GetStudentsFromVariants(string directory)
        {
            var newPath = directory + "\\Variants";
            if (!Directory.Exists(newPath)) throw new InvalidOperationException("No such directory or invalid directory format!");
            var files = Directory.GetFiles(newPath, "*.txt");
            var students = new List<Student>();

            foreach (var file in files)
                students.Add(GetStudentFromFile(file));

            return students;
        }

        private Student GetStudentFromFile(string path)
        {
            var splited = path.Split('\\');
            var name = splited.Last().Split('.').FirstOrDefault();
            string line;
            var exercises = new List<string>();
            try
            {
                StreamReader sr = new StreamReader(path);
                line = sr.ReadLine();
                while (line != null)
                {
                    exercises.Add(line);
                    line = sr.ReadLine();
                }
                sr.Close();
            }
            catch (Exception e)
            {
                _OutPut.PrintWithRed($"Exception on student {name}: " + e.Message);
            }

            splited[splited.Length - 2] = "Students";
            splited[splited.Length - 1] = name;

            return new Student() { Name = name, Exercises = exercises, PathToDllFolder = string.Join('\\', splited) };
        }
    }
}   