﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyberpunk_2088
{
    class OutPut : IOutPut
    {

        public  void PrintWithRed(string message, bool saveColor = false)
        {
            var color = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Red;
            Print(message);
            if (!saveColor)
                Console.ForegroundColor = color;
        }

        public void PrintWithYellow(string message, bool saveColor = false)
        {
            var color = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Yellow;
            Print(message);
            if (!saveColor)
                Console.ForegroundColor = color;
        }

        public  void Print(string message)
        {
            Console.WriteLine(message);
        }

        public void PrintF(string message)
        {
            Console.Write(message);
        }

        public  void PrintFWithRed(string message, bool saveColor = false)
        {
            var color = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Red;
            PrintF(message);
            if (!saveColor)
                Console.ForegroundColor = color;
        }

        public void PrintFWithGreen(string message, bool saveColor = false)
        {
            var color = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Green;
            PrintF(message);
            if (!saveColor)
                Console.ForegroundColor = color;
        }
    }

    public interface IOutPut
    {
        void PrintWithRed(string message, bool saveColor = false);

        void PrintWithYellow(string message, bool saveColor = false);
        void Print(string message);

        void PrintF(string message);

        void PrintFWithRed(string message, bool saveColor = false);

        void PrintFWithGreen(string message, bool saveColor = false);
    }
}
