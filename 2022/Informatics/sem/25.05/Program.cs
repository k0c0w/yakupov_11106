﻿namespace Cyberpunk_2088
{
    internal class Program
    {
        static void Main()
        {
            Console.WriteLine("Tests are available only with strict format. Read \"Format.txt\"");
            Console.WriteLine();
            Console.WriteLine("Enter main folder fullpath: ");
            var directory = Console.ReadLine();
            if (directory == null) throw new NullReferenceException();

            Console.WriteLine("Enter amount of threads: ");
            int threadsAmount;
            while (!int.TryParse(Console.ReadLine(), out threadsAmount) && threadsAmount <= 0) ;

            var adapter = new TestAdapter(new OutPut());
            adapter.ExecuteTests(directory, threadsAmount);
        }
    }
}
