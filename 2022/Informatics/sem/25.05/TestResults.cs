﻿namespace Cyberpunk_2088
{
    class TestResults
    {
        public readonly string StudentName;
        
        public readonly List<Failure> Failures = new List<Failure>();
        public readonly List<TestResult> Tests = new List<TestResult>();

        public TestResults(string student)
        {
            StudentName = student;
        }
    }

    record class TestResult
    {
        public string TestName { get; init; }
        public int Success { get; init; }
        public int Failure { get; init; }
    }

    record class Failure
    {
        public string? TestName { get; init; }

        public FailureType Type { get; init; }

        public IReadOnlyList<string>? Parameters { get; init; }

        public Exception? Exception { get; init; }
    }

    enum FailureType
    {
        WrongAnswer,
        NoSuchStructure,
        ThworsException,
        NoSuchDll
    }
}
