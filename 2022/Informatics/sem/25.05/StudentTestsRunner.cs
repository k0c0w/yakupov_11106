﻿using System.Collections.ObjectModel;
using System.Reflection;


namespace Cyberpunk_2088
{
    internal class StudentTestsRunner
    {
        public Student student { get; init; }
        ReadOnlyDictionary<string, List<string>> sturctureInfo;
        ReadOnlyDictionary<string, List<List<string>>> allTests;
        readonly IOutPut _OutPut;

        public StudentTestsRunner(Student student, 
            ReadOnlyDictionary<string, List<string>> exerciseInfo, 
            ReadOnlyDictionary<string, List<List<string>>> allTestCases, IOutPut output)
        {
            this.student = student;
            sturctureInfo = exerciseInfo;
            allTests = allTestCases;
            _OutPut = output;
        }

        public TestResults RunTests()
        {
            var studentResults = new TestResults(student.Name);
            foreach(var exercise in student.Exercises)
            {
                var structureShould = sturctureInfo[exercise];
                var dll = string.Format($"{student.PathToDllFolder}\\{exercise}.dll");
                try
                {
                    var assembly = Assembly.LoadFile(dll);
                    var testName = structureShould?.FirstOrDefault();
                    var succses = 0;
                    var failure = 0;

                    foreach (var testCase in allTests[exercise])
                    {
                        object? result = null;
                        try
                        {
                            result = ExecuteTest(exercise, testCase, structureShould, assembly);
                        }
                        catch (ElementNotFoundExeption ex)
                        {
                            failure++;
                            studentResults.Failures.Add(new Failure()
                            {
                                TestName = testName,
                                Type = FailureType.NoSuchStructure,
                            });
                            continue;
                        }
                        catch (Exception ex)
                        {
                            failure++;
                            studentResults.Failures.Add(new Failure()
                            {
                                TestName = testName,
                                Exception = ex,
                                Type = FailureType.ThworsException,
                                Parameters = testCase
                            });
                            continue;
                        }

                        if (IsCorrect(result, testCase.Last(), structureShould[2]))
                            succses++;
                        else
                            failure++;
                    }

                    studentResults.Tests.Add(new TestResult()
                    { Failure = failure, Success = succses, TestName = testName });
                }
                catch (FileNotFoundException ex)
                {
                    studentResults.Failures.Add(new Failure { TestName = exercise, Type = FailureType.NoSuchDll });
                    studentResults.Tests.Add(new TestResult() { TestName = exercise, Failure = 1 });
                }
                catch(Exception ex)
                {
                    _OutPut.PrintWithRed(ex.Message);
                    continue;
                }
            }

            return studentResults;
        }

        bool IsCorrect(object? result, string expected, string type)
        {
            if(result == null) return false;

            else if (type == "double")
                return (double)result == double.Parse(expected);
            else if (type == "int")
                return (int)result == int.Parse(expected);
            else if (type == "string")
                return (string)result == expected;
            else if(type == "bool")
                return (bool)result == bool.Parse(expected);
            else if (type == "char")
                return (char)result == char.Parse(expected);

            return false;
        }

        object ExecuteTest(string @namespace, List<string> testCase,  List<string> structureShould,  Assembly assembly)
        {

            var @class = assembly.GetType($"{@namespace}.{structureShould[1].Split('.').First()}");
            var method = @class?.GetMethod(structureShould[1].Split('.').Last(), BindingFlags.Instance | BindingFlags.Public);
            bool correctParams = ParamsAreCorrect(method, structureShould);
            bool corretReturnType = ReturnParamIsCorrect(structureShould[2], method?.ReturnParameter.ParameterType);
            if (@class == null || method == null || !correctParams || !corretReturnType)
                throw new ElementNotFoundExeption();

            var parameters = GetConvertedParams(testCase, structureShould);
            var instance = @class.GetConstructor(new Type[] {}).Invoke(new object[] {});
            return  method.Invoke(instance, parameters);
        }

        object[] GetConvertedParams(List<string> testCase, List<string> structureShould)
        {
            var result = new List<object>();
            var step = 0;
            for(int i = 3; i < structureShould.Count; i++)
            {
                var param = structureShould[i].Split(':').Last();
                var value = testCase[step].Split(':').Last();
                object obj = null;

                if (param == "int")
                    obj = int.Parse(value);
                else if (param == "double")
                    obj = double.Parse(value);
                else if (param == "char")
                    obj = char.Parse(value);
                else if (param == "bool")
                    obj = bool.Parse(value);
                else if (param == "string")
                    obj = value;
                result.Add(obj);
                step++;
            }
            return result.ToArray();
        }

        bool ReturnParamIsCorrect(string param, Type? type)
        {
            if (type == null) return false;
            if (param == "int")
                return type == typeof(int);
            else if (param == "double")
                return type == typeof(double);
            else if (param == "char")
                return type == typeof(char);
            else if (param == "bool")
                return type == typeof(bool);
            else if (param == "string")
                return type == typeof(string);
            return false;
        }

        bool ParamsAreCorrect(MethodInfo? method, List<string> structureShould)
        {
            if (method == null) return false;
            
            var parameters = method.GetParameters();
            if(parameters.Length != structureShould.Count - 3 ) return false;

            int paramStep = 0;
            for(var i = 3; i < structureShould.Count; i++)
            {
                var param = structureShould[i].Split(':').Last();

                if(param == "int" && parameters[paramStep].ParameterType != typeof(int))
                    return false;
                else if (param == "double" && parameters[paramStep].ParameterType != typeof(double))
                    return false;
                else if (param == "char" && parameters[paramStep].ParameterType != typeof(char))
                    return false;
                else if (param == "bool" && parameters[paramStep].ParameterType != typeof(bool))
                    return false;
                else if (param == "string" && parameters[paramStep].ParameterType != typeof(string))
                    return false;
                paramStep++;
            }
            return true;
        }
    }

    class ElementNotFoundExeption : Exception
    {

    }
}
