namespace Cyberpunk_2088
{

    public static class LinqExtension
    {
    public class Program
    {
        static void Main()
        {
            var a = new[] { 1, 2, 3, 4 };


            int count = 5;

            var f = a.Skip(count)
                .Select(x => x)
                .ToList();

            f.AddRange(a.Take(count));
            a = f.ToArray();

        }
    }
}
