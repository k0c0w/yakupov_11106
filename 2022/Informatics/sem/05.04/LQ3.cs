namespace Cyberpunk_2088
{
    public class Program
    {
        static void Main()
        {
            var input = Console.ReadLine().Split();
            var result = input.Select(word => word)
			      .Where(word => 
			word.GroupBy(x => x).All(group => group.Count() <= 2))
			      .ToArray();
            foreach(var word in result)
            {
                Console.WriteLine(word);
            }
        }
    }
}
