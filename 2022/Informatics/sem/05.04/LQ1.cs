namespace Cyberpunk_2088
{

    public static class LinqExtension
    {
        public static IEnumerable<Point> GetNeighbours<T>(this IEnumerable<T> array, Point p)
        {
            int[] d = { -1, 0, 1 };
            return d
                .SelectMany(x => d.Select(y => new Point(p.X + x, p.Y + y)))
                .Where(newPoint => !p.Equals(newPoint));
        }
    }
    public class Point
    {
        public int X { get; init; }
        public int Y { get; init; }

        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        public override string ToString()
        {
            return String.Format("({0},{1})", X, Y);
        }

        public override bool Equals(object? obj)
        {
            if(obj != null && obj is Point)
            {
                var p = (Point)obj;
                return X == p.X && Y == p.Y;
            }
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return X * 137 ^ Y * 137;
        }
    }

    public class Program
    {
        static void Main()
        {
            var array = new[] { new Point(0, 0) , new Point(0, 2)};



            var a = array.SelectMany(x => array.GetNeighbours(x))
            .GroupBy(x => x)
            .Count();
            Console.WriteLine(a);
        }
}