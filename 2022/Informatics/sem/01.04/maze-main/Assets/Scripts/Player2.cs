using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player2 : MonoBehaviour
{
    [SerializeField] KeyCode KeyRight;
    [SerializeField] KeyCode KeyLeft;
    [SerializeField] KeyCode KeyUp;
    [SerializeField] KeyCode KeyDown;
    [SerializeField] public KeyCode KeyShot;

    public int Health = 2;
    public GameObject deathEffect;

    private float LastRotation;

    private float acceleration = 3;
    private Rigidbody2D rigidBodyComponent;
    // Start is called before the first frame update
    void Start()
    {
        rigidBodyComponent = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame

    private void FixedUpdate()
    {
        MakeMovement();
        if (Input.GetKey(KeyShot))
        {
            MakeShot();
        }
    }

    void Update()
    {

    }
    public void TakeDamage(int damage)
    {
        Health -= damage;
        if (Health <= 0)
        {
            Die();
        }
    }

    void Die()
    {
        Instantiate(deathEffect, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
    private void MakeShot()
    {
        //Instantiate()
    }

    private void MakeMovement()
    {
        var right = 0;
        var left = 0;
        var up = 0;
        var down = 0;
        if (Input.GetKey(KeyRight))
        {
            right = 1;
        }
        if (Input.GetKey(KeyLeft))
        {
            left = -1;
        }
        if (Input.GetKey(KeyUp))
        {
            up = 1;
        }
        if (Input.GetKey(KeyDown))
        {
            down = -1;
        }
        var movement = new Vector2((left + right), up + down);
        rigidBodyComponent.velocity = acceleration * movement;
        if (left + right != 0 || up + down != 0)
        {
            LastRotation = movement.GetAngle();
        }
        transform.rotation = Quaternion.Euler(0, 0, LastRotation);
    }
}
