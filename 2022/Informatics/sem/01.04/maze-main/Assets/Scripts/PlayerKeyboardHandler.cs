﻿using System;
using UnityEngine;

public class PlayerKeyboardHandler : MonoBehaviour
{
    [SerializeField] KeyCode KeyRight;
    [SerializeField] KeyCode KeyLeft;
    [SerializeField] KeyCode KeyUp;
    [SerializeField] KeyCode KeyDown;
    private float acceleration = 3;
    private Rigidbody2D rigidBodyComponent;

    private void Start()
    {
        rigidBodyComponent = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        var right = 0;
        var left = 0;
        var up = 0;
        var down = 0;
        if (Input.GetKey(KeyRight))
        {
            right = 1;
        }
        if (Input.GetKey(KeyLeft))
        {
            left = -1;
        }
        if (Input.GetKey(KeyUp))
        {
            up = 1;
        }
        if (Input.GetKey(KeyDown))
        {
            down = -1;
        }
        var movement = new Vector2((left + right), up + down);
        rigidBodyComponent.velocity =acceleration *  movement;
        transform.rotation = Quaternion.Euler(0, 0, movement.GetAngle());
    }
    private void Update()
    {
    }

    void OnCollisionEnter(Collision coll)
    {
        if (coll.gameObject.CompareTag("Player"))
        {
            Destroy(this);
        }
    }
}
